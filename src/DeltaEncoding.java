import java.util.ArrayList;

public class DeltaEncoding {

    public static void main(String[] args) {

    }

    public int[] delta_encode(int[] list) {

        if (list == null || list.length == 0) {
            return list;
        }

        int size = list.length;
        ArrayList<Integer> result = new ArrayList<>();
        result.add(list[0]);

        for (int i = 1; i < size; i++) {

            int diff = list[i] - list[i - 1];
            if (Math.abs(diff) > 127) {

                result.add(-128);
            }
            result.add(diff);

        }

        return result.stream()
                     .mapToInt(i -> i)
                     .toArray();
    }

}