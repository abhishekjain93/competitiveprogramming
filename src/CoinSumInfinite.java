import java.util.ArrayList;

public class CoinSumInfinite {

    public int coinchange2(ArrayList<Integer> coins, int sum) {

        int[][] dp = new int[sum + 1][coins.size()];

        for (int i = 1; i < coins.size(); i++) {

            dp[0][i] = 1;
        }
        for (int j = 1; j <= sum; j++) {

            dp[j][0] = j % coins.get(0) == 0 ? 1 : 0;
        }

        for (int i = 1; i < coins.size(); i++) {
            for (int j = 1; j <= sum; j++) {

                if (j < coins.get(i))
                    dp[j][i] = dp[j][i - 1];
                else
                    dp[j][i] = (dp[j - coins.get(i)][i] + dp[j][i - 1])% 1000007;
            }

        }

        return dp[sum][coins.size() - 1]% 1000007;
    }


    public int coinchange(ArrayList<Integer> coins, int amount) {
        int[][] dp = new int[coins.size()+1][amount+1];
        dp[0][0] = 1;

        for (int i = 1; i <= coins.size(); i++) {
            dp[i][0] = 1;
            for (int j = 1; j <= amount; j++) {
                dp[i][j] = (dp[i-1][j] + (j >= coins.get(i-1) ? dp[i][j-coins.get(i-1)] : 0))% 1000007;
            }
        }
        return dp[coins.size()][amount]% 1000007;
    }

}
