import java.util.*;

public class ABCDEqual {

	public ArrayList<Integer> equal(ArrayList<Integer> a) {

		int size = a.size();
		if (size < 4)
			return null;
		ArrayList<Integer> res = new ArrayList<>(
				Arrays.asList(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE));
		HashMap<Integer, ArrayList<ArrayList<Integer>>> map = new HashMap<>();
		for (int i = 0; i < size - 1; i++) {
			for (int j = i + 1; j < size; j++) {
				int sum = a.get(i) + a.get(j);
				if (map.containsKey(sum)) {

					ArrayList<ArrayList<Integer>> tuple = map.get(sum);
					tuple.add(new ArrayList<>(Arrays.asList(i, j)));

				} else {
					ArrayList<ArrayList<Integer>> tuple = new ArrayList<>();
					tuple.add(new ArrayList<>(Arrays.asList(i, j)));
					map.put(sum, tuple);

				}
			}
		}

		for (Integer s : map.keySet()) {

			if (map.get(s).size() == 1)
				continue;

			ArrayList<ArrayList<Integer>> values = map.get(s);
			Collections.sort(values, new ListComparator<>());

			ArrayList<Integer> possible = new ArrayList<>(Arrays.asList(values.get(0).get(0), values.get(0).get(1)));

			for (int i = 1; i < values.size(); ) {

				if (possible.contains(values.get(i).get(0)) || possible.contains(values.get(i).get(1))) {
					values.remove(i);
					continue;
				}
				i++;
			}
			if (values.size() < 2)
				continue;

			possible.add(values.get(1).get(0));
			possible.add(values.get(1).get(1));

			res = maxim(res, possible);
		}

		return res;
	}

	private ArrayList<Integer> maxim(ArrayList<Integer> res, ArrayList<Integer> possible) {

		for (int i = 0; i < 4; i++) {

			if (possible.get(i) < res.get(i))
				return possible;
			if (possible.get(i) > res.get(i))
				return res;

		}

		return res;
	}

	public static void main(String[] args) {

		System.out.println(new ABCDEqual().equal(new ArrayList<>(Arrays.asList(3, 4, 7, 1, 2, 9, 8))));

	}

	static class ListComparator<T extends Comparable<T>> implements Comparator<List<T>> {

		@Override
		public int compare(List<T> o1, List<T> o2) {
			for (int i = 0; i < Math.min(o1.size(), o2.size()); i++) {
				int c = o1.get(i).compareTo(o2.get(i));
				if (c != 0) {
					return c;
				}
			}
			return Integer.compare(o1.size(), o2.size());
		}

	}

}
