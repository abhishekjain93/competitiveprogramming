import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MinSumPath {
    public static void main(String[] args) {


        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();

        matrix.add(new ArrayList<>(Arrays.asList(1, 3, 2)));
        matrix.add(new ArrayList<>(Arrays.asList(4, 3, 1)));
        matrix.add(new ArrayList<>(Arrays.asList(5, 6, 1)));

        System.out.println(new MinSumPath().minPathSum(matrix));

    }

    public int minPathSum(ArrayList<ArrayList<Integer>> matrix) {

        int rows = matrix.size();
        int cols = matrix.get(0).size();
        ArrayList res = new ArrayList<Integer>(rows + cols);

        int[][] dp = new int[rows][cols];
        dp[0][0] = matrix.get(0).get(0);

        MinSumPath.Pair[][] parent = new MinSumPath.Pair[rows][cols];
        parent[0][0] = new MinSumPath.Pair(0, 0);

        for (int i = 1; i < cols; i++) {

            dp[0][i] = dp[0][i - 1] + matrix.get(0).get(i);
            parent[0][i] = new MinSumPath.Pair(0, i - 1);
        }

        for (int j = 1; j < rows; j++) {

            dp[j][0] = dp[j - 1][0] + matrix.get(j).get(0);
            parent[j][0] = new MinSumPath.Pair(j - 1, 0);
        }

        for (int i = 1; i < rows; i++) {

            for (int j = 1; j < cols; j++) {

                dp[i][j] = matrix.get(i).get(j) + Math.min(dp[i - 1][j], dp[i][j - 1]);

                parent[i][j] = dp[i - 1][j] < dp[i][j - 1] ? new MinSumPath.Pair(i - 1, j) : new MinSumPath.Pair(i, j - 1);

            }
        }

        //System.out.println(Arrays.deepToString(parent));
        int i = rows - 1, j = cols - 1;
        res.add(matrix.get(i).get(j));

        while (i != 0 || j != 0) {

            MinSumPath.Pair pair = parent[i][j];
            res.add(matrix.get(pair.i).get(pair.j));
            i = pair.i;
            j = pair.j;
        }

        Collections.reverse(res);

        return dp[rows-1][cols-1];
    }

    private class Pair {

        int i;
        int j;

        public Pair(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public String toString() {
            return "" + i + ":" + j;

        }
    }
}
