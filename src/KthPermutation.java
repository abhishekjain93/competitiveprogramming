public class KthPermutation {


    public static void main(String[] args) {

        System.out.println(new KthPermutation().getPermutation(4, 24));
    }

    public int factorial(int n) {

        if (n > 12) return Integer.MAX_VALUE;
        if (n < 2) return 1;
        return n * factorial(n - 1);

    }

    public String getPermutation(int n, int k) {

        if (n <= 0 || k <= 0) return "";
        k--;
        int arr[] = new int[n];

        for (int i = 0; i < n; i++) {

            arr[i] = i + 1;
        }

        StringBuilder sb = new StringBuilder();
        int idx = 0;
        int fact = factorial(n);
        if (k > fact) return "";

        for (int i = 0; i < n; i++) {

            fact = factorial(n - i - 1);
            sb.append(arr[idx + k / fact]);
            shiftArr(arr, idx, idx + k / fact);
            k = k % fact;
            idx++;
        }

        return sb.toString();
    }

    private void shiftArr(int[] arr, int start, int pos) {

        System.arraycopy(arr, start, arr, start + 1, pos - start);

    }

}
