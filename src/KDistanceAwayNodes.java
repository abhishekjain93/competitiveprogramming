import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class KDistanceAwayNodes {

    public List<TreeNode> atKDistance(TreeNode root, TreeNode node, int k) {

        List<TreeNode> result = new ArrayList<>();
        atKDistance(root, node, k, result);

        return result;
    }

    private int atKDistance(TreeNode root, TreeNode node, int k, List<TreeNode> result) {

        if (root == null) return -1;

        if (root.val == node.val) {

            result.addAll(atKDistanceDown(node, k));
            return 0;
        }

        final int leftDepth = atKDistance(root.left, node, k, result);

        if (leftDepth != -1) {

            if (leftDepth == k - 1) {
                result.add(root);

            } else if (leftDepth < k - 1) {

                result.addAll(atKDistanceDown(root.right, k - leftDepth - 2));

            }

            return 1 + leftDepth;
        }

        final int rightDepth = atKDistance(root.right, node, k, result);

        if (rightDepth != -1) {

            if (leftDepth == k - 1) {
                result.add(root);

            } else if (rightDepth < k - 1) {

                result.addAll(atKDistanceDown(root.left, k - rightDepth - 2));

            }

            return 1 + rightDepth;
        }

        return -1;
    }

    private List<TreeNode> atKDistanceDown(TreeNode node, int k) {

        if (node == null) return Collections.emptyList();
        if (k == 0) {

            return Collections.singletonList(node);

        }
        List<TreeNode> result = new ArrayList<>();
        result.addAll(atKDistanceDown(node.left, k - 1));
        result.addAll(atKDistanceDown(node.right, k - 1));
        return result;
    }

}
