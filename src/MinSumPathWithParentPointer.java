import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MinSumPathWithParentPointer {


    public static void main(String[] args) {


        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();

        matrix.add(new ArrayList<>(Arrays.asList(20, 29, 84, 4, 32, 60, 86, 8, 7, 37)));
        matrix.add(new ArrayList<>(Arrays.asList(77, 69, 85, 83, 81, 78, 22, 45, 43, 63)));
        matrix.add(new ArrayList<>(Arrays.asList(60, 21, 0, 94, 59, 88, 9, 54, 30, 80)));

        matrix.add(new ArrayList<>(Arrays.asList(40, 78, 52, 58, 26, 84, 47, 0, 24, 60)));
        matrix.add(new ArrayList<>(Arrays.asList(40, 17, 69, 5, 38, 5, 75, 59, 35, 26)));
        matrix.add(new ArrayList<>(Arrays.asList(64, 41, 85, 22, 44, 25, 3, 63, 33, 13)));

        matrix.add(new ArrayList<>(Arrays.asList(2, 21, 39, 51, 75, 70, 76, 57, 56, 22)));
        matrix.add(new ArrayList<>(Arrays.asList(31, 45, 47, 100, 65, 10, 94, 96, 81, 14)));

        System.out.println(new MinSumPathWithParentPointer().minSumPath(matrix));

    }

    public ArrayList minSumPath(ArrayList<ArrayList<Integer>> matrix) {

        int rows = matrix.size();
        int cols = matrix.get(0).size();
        ArrayList res = new ArrayList<Integer>(rows + cols);

        int[][] dp = new int[rows][cols];
        dp[0][0] = matrix.get(0).get(0);
        Pair[][] parent = new Pair[rows][cols];
        parent[0][0] = new Pair(0, 0);

        for (int i = 1; i < cols; i++) {

            dp[0][i] = dp[0][i - 1] + matrix.get(0).get(i);
            parent[0][i] = new Pair(0, i - 1);
        }

        for (int j = 1; j < rows; j++) {

            dp[j][0] = dp[j - 1][0] + matrix.get(j).get(0);
            parent[j][0] = new Pair(j - 1, 0);
        }

        for (int i = 1; i < rows; i++) {

            for (int j = 1; j < cols; j++) {

                dp[i][j] = matrix.get(i).get(j) + Math.min(dp[i - 1][j], dp[i][j - 1]);

                parent[i][j] = dp[i - 1][j] < dp[i][j - 1] ? new Pair(i - 1, j) : new Pair(i, j - 1);

            }
        }

        System.out.println(Arrays.deepToString(dp));
        int i = rows - 1, j = cols - 1;
        res.add(matrix.get(i).get(j));

        while (i != 0 || j != 0) {

            Pair pair = parent[i][j];
            res.add(matrix.get(pair.i).get(pair.j));
            i = pair.i;
            j = pair.j;
        }

        Collections.reverse(res);

        return res;
    }

    private class Pair {

        int i;
        int j;

        public Pair(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public String toString() {
            return "" + i + ":" + j;

        }
    }
}
