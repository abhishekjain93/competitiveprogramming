import java.util.ArrayList;

public class PathSumII {

    public ArrayList<ArrayList<Integer>> pathSum(TreeNode root, int sum) {

        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        ArrayList<Integer> curr = new ArrayList<>();
        pathHelper(root, sum, curr, res);

        return res;

    }

    private void pathHelper(TreeNode root, int sum, ArrayList<Integer> curr, ArrayList<ArrayList<Integer>> res) {

        if (root == null) return;

        curr.add(root.val);

        if (root.left == null && root.right == null)
            if (sum - root.val == 0) {

                res.add(new ArrayList<>(curr));
                curr.remove(curr.size() - 1);
                return;

            } else {

                curr.remove(curr.size() - 1);
                return;

            }

        pathHelper(root.left, sum - root.val, curr, res);
        pathHelper(root.right, sum - root.val, curr, res);
        curr.remove(curr.size() - 1);

    }

}
