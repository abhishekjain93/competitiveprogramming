import java.util.Arrays;

public class Quicksort {

	public void qsort(int[] a, int low, int high) {

		if (low < high) {

			int p = partition(a, low, high);

			qsort(a, low, p);
			qsort(a, p + 1, high);

		}

	}

	private int partition(int[] a, int low, int high) {

		int pivot = a[low];
		int i = low - 1;
		int j = high + 1;

		while (true) {

			do
				i++;
			while (a[i] < pivot);

			do
				j--;
			while (a[j] > pivot);

			if (i >= j)
				return j;

			int tmp = a[i];
			a[i] = a[j];
			a[j] = tmp;

		}

	}

	public static void main(String[] args) {

		int[] a = { 5, 1, 2, 9, 3, 6, 7, 6 };
		new Quicksort().qsort(a, 0, a.length - 1);
		System.out.println(Arrays.toString(a));
	}
}
