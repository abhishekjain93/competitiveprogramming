public class ListCycle {

    public ListNode detectCycle(ListNode A) {

        if (A == null || A.next == null) return null;

        ListNode slowPtr = A.next;
        ListNode fastPtr = A.next.next;

        while (fastPtr != null && fastPtr.next != null) {

            if (slowPtr == fastPtr) {

                fastPtr = A;

                while (slowPtr != fastPtr) {

                    slowPtr = slowPtr.next;
                    fastPtr = fastPtr.next;

                }
                return slowPtr;

            }

            slowPtr = slowPtr.next;
            fastPtr = fastPtr.next.next;

        }
        return null;
    }

}
