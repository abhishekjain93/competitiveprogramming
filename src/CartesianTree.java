import java.util.ArrayList;
import java.util.Collections;

public class CartesianTree {


    public TreeNode buildTree(ArrayList<Integer> sequence) {

        if (sequence.size() == 0)
            return null;
        int min_index = sequence.indexOf(Collections.max(sequence));

        TreeNode root = new TreeNode(sequence.get(min_index));
        root.left = buildTree((new ArrayList<>(sequence.subList(0, min_index))));
        root.right = buildTree(new ArrayList<>(sequence.subList(min_index + 1, sequence.size())));

        return root;
    }
}
