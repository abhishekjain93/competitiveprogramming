import java.util.Arrays;
import java.util.List;

public class MaximumDiff {
	public int maxArr(List<Integer> A) {

		int size = A.size();
		int ul, ur, bl, br;
		ul = ur = bl = br = Integer.MIN_VALUE;

		for (int i = 1; i <= size; i++) {

			ur = Math.max(ur, A.get(i - 1) + i);
			ul = Math.max(ul, A.get(i - 1) - i);
			br = Math.max(br, -A.get(i - 1) + i);
			bl = Math.max(bl, -A.get(i - 1) - i);

		}
		int max = Integer.MIN_VALUE;

		for (int i = 1; i <= size; i++) {

			max = Math.max(max, ur - A.get(i - 1) - i);
			max = Math.max(max, ul - A.get(i - 1) + i);
			max = Math.max(max, br + A.get(i - 1) - i);
			max = Math.max(max, bl + A.get(i - 1) + i);
		}

		return max;

	}

	public static void main(String[] args) {

		System.out.println(new MaximumDiff().maxArr(Arrays.asList(2, 2, 2)));

	}
}