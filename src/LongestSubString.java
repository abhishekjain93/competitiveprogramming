import java.util.HashMap;
import java.util.Map;

public class LongestSubString {

	public String longestSubstring(String a) {

		int length = a.length();
		Map<Character, Integer> lookup = new HashMap<>();

		int i = 0, start = 0, count = 0, maxcount = 0, maxstart = 0;

		while (i < length) {

			if (lookup.get(a.charAt(i)) == null) {
				lookup.put(a.charAt(i), i);
				count++;
				i++;

			} else {

				if (count > maxcount) {
					maxcount = count;
					maxstart = start;

				}
				int cutoff = lookup.get(a.charAt(i));
				for (int j = start; j <= cutoff; j++) {

					lookup.remove(a.charAt(j));

				}
				start = cutoff + 1;
				count = i - start + 1;
				lookup.put(a.charAt(i), i);
				i++;
			}
		}
		if (count > maxcount) {
			maxcount = count;
		}
		System.out.println(maxstart + "  " + maxcount);
		return a.substring(maxstart, maxstart + maxcount);
	}

	public static void main(String[] args) {

		System.out.println(new LongestSubString().longestSubstring(
				"bbbbbbb"));
	}

}
