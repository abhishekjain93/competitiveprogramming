import java.util.Stack;

public class Sum2BinaryTree {

    public static void main(String[] args) {

        TreeNode root = new TreeNode(10);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);

        System.out.println(new Sum2BinaryTree().t2Sum1(root, 19));
        System.out.println(new Sum2BinaryTree().t2Sum(root, 19));

    }


    public int t2Sum1(TreeNode root, int k) {

        if (root == null) return 0;
        while (root.val > k)
            root = root.left;

        Stack<TreeNode> leftPtr = new Stack<>();
        Stack<TreeNode> rightPtr = new Stack<>();

        for (TreeNode cur = root; cur != null; cur = cur.left)
            leftPtr.push(cur);
        for (TreeNode cur = root; cur != null; cur = cur.right)
            rightPtr.push(cur);

        while (!leftPtr.empty() && !rightPtr.empty() && leftPtr.peek() != rightPtr.peek()) {
            int tmpSum = leftPtr.peek().val + rightPtr.peek().val;
            if (tmpSum == k) return 1;
            else if (tmpSum < k)
                for (TreeNode cur = leftPtr.pop().right; cur != null; cur = cur.left)
                    leftPtr.push(cur);
            else
                for (TreeNode cur = rightPtr.pop().left; cur != null; cur = cur.right)
                    rightPtr.push(cur);
        }

        return 0;
    }

    public int t2Sum(TreeNode root, int k) {

        if (root == null) return 0;
        while (root.val > k)
            root = root.left;

        Stack<TreeNode> leftPtr = new Stack<>();
        Stack<TreeNode> rightPtr = new Stack<>();

        TreeNode left = root, right = root;

        while (left != null) {
            leftPtr.push(left);
            left = left.left;
        }
        while (right != null) {
            rightPtr.push(right);
            right = right.right;
        }

        while (!leftPtr.empty() && !rightPtr.empty() && leftPtr.peek() != rightPtr.peek()) {

            int tempSum = leftPtr.peek().val + rightPtr.peek().val;
            if (tempSum == k) return 1;
            else if (tempSum < k) {

                TreeNode node = leftPtr.pop().right;
                while (node != null) {
                    leftPtr.push(node);
                    node = node.left;
                }
            } else {

                TreeNode node = rightPtr.pop().left;
                while (node != null) {
                    rightPtr.push(node);
                    node = node.right;
                }
            }
        }

        return 0;
    }


}
