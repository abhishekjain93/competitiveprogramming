import java.util.Arrays;

public class ShortestUniquePrefix {

    public static void main(String[] args) {

        System.out.println(Arrays.toString(new ShortestUniquePrefix().prefix(new String[]{"zebra", "dog",
                "duck", "dove"})));

    }

    public String[] prefix(String[] words) {

        Trie trie = new Trie();

        for (String word : words) {

            trie.insert(word);

        }

        return Arrays.stream(words)
                     .map(word -> trie.geUniquePrefix(word))
                     .toArray(String[]::new);

    }

    class TrieNode {

        boolean wordEnd;
        String word;
        boolean overlapped;
        TrieNode[] links = new TrieNode[26];

    }

    class Trie {

        TrieNode root = new TrieNode();

        public void insert(String word) {

            TrieNode node = root;
            for (Character c : word.toCharArray()) {

                if (node.links[c - 'a'] == null) {

                    node.links[c - 'a'] = new TrieNode();
                } else {

                    node.links[c - 'a'].overlapped = true;
                }

                node = node.links[c - 'a'];
            }

            node.wordEnd = true;
            node.word = word;
        }

        public String geUniquePrefix(String word) {

            TrieNode node = root;
            StringBuilder sb = new StringBuilder();

            for (Character c : word.toCharArray()) {

                sb.append(c);
                if (node.links[c - 'a'] == null) {

                    return "";

                } else if (!node.links[c - 'a'].overlapped) {

                    return sb.toString();
                }

                node = node.links[c - 'a'];
            }

            return sb.toString();
        }

    }

}
