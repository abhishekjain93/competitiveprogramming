public class ABNot3Consecutive {

    public static void main(String[] args) {

        // System.out.println(new ABNot3Consecutive().formString(1, 4));

        int[] x = new int[]{11, 10, 10, 5, 10, 15, 20, 10, 7, 11};

        System.out.println(new ABNot3Consecutive().foo(x, 8, 18, 3, 6));
        System.out.println(new ABNot3Consecutive().foo(x, 10, 20, 0, 9));
        System.out.println(new ABNot3Consecutive().foo(x, 8, 18, 6, 3));
        System.out.println(new ABNot3Consecutive().foo(x, 20, 10, 0, 9));
        System.out.println(new ABNot3Consecutive().foo(x, 6, 7, 8, 8));

    }

    public String formString(int A, int B) {

        StringBuilder res = new StringBuilder();
        while (A != 0 || B != 0) {

            if (A > B) {

                if (B > 0) {

                    res.append("aab");
                    A -= 2;
                    B -= 1;

                } else {

                    if (A > 2) return "";
                    res.append(A == 2 ? "aa" : "a");
                    return res.toString();
                }

            } else if (A == B) {

                if (res.length() > 0 && res.charAt(res.length() - 1) == 'a') {

                    while (A != 0) {

                        res.append("ba");
                        A -= 1;
                        B -= 1;

                    }

                } else {
                    while (A != 0) {

                        res.append("ab");
                        A -= 1;
                        B -= 1;

                    }
                }

                return res.toString();

            } else {

                if (A > 0) {

                    res.append("bba");
                    B -= 2;
                    A -= 1;

                } else {

                    if (B > 2) return "";
                    res.append(B == 2 ? "bb" : "b");
                    return res.toString();
                }

            }

        }

        return res.toString();
    }

    public int foo(int[] x, int a, int b, int i, int j) {

        int k = j;
        int ct = 0;

        while (k > i - 1) {

            if (x[k] <= b && !(x[k] <= a)) {
                ct++;
            }
            k--;

        }
        return ct;
    }

}
