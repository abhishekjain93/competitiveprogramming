public class SymmetricTree {

    public int isSymmetric(TreeNode root) {

        if (root == null) return 1;
        return isSymmetric(root.left, root.right);

    }

    public int isSymmetric(TreeNode a, TreeNode b) {

        if (a == null && b == null) return 1;
        if (a == null || b == null) return 0;
        if (a.val == b.val) return isSymmetric(a.left, b.right) & isSymmetric(a.right, b.left);

        return 0;
    }
}
