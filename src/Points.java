import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Points {
	public int maxPoints(ArrayList<Integer> a, ArrayList<Integer> b) {

		int size = a.size();
		int max = 0;
		if (size == 1)
			return 1;
		HashMap<Double, Integer> map = new HashMap<>();
		for (int i = 0; i < size - 1; i++) {
			int px = a.get(i);
			int py = b.get(i);
			int duplicate = 0, vertical = 0, maxforpoint = 0;
			map.clear();
			for (int j = i + 1; j < size; j++) {

				int tx = a.get(j);
				int ty = b.get(j);

				if (px == tx) {
					if (py == ty)
						duplicate++;
					else
						vertical++;
					continue;
				}
				double slope = (ty==py)?0.0:(1.0 * (ty - py)) / (tx - px);
				if (map.get(slope) != null) {
					map.put(slope, map.get(slope) + 1);

				} else {
					map.put(slope, 1);
				}

				if (map.get(slope) > maxforpoint)
					maxforpoint = map.get(slope);

			}
			if (vertical > maxforpoint)
				maxforpoint = vertical;

			if (maxforpoint + duplicate + 1 > max)
				max = maxforpoint + duplicate + 1;

		}
		return max;
	}

	public static void main(String[] args) {
		System.out.println(new Points().maxPoints(new ArrayList<Integer>(Arrays.asList(4, 8, -4)),
				new ArrayList<>(Arrays.asList(-4, -4, -4))));
		;
	}
}