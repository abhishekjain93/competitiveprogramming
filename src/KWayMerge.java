import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;

public class KWayMerge {

    public static void main(String[] args) {

        ArrayList<ArrayList<Integer>> lists = new ArrayList<>();
        lists.add(new ArrayList<>(Arrays.asList(1, 2, 5, 7, 9)));
        lists.add(new ArrayList<>(Arrays.asList(1, 3, 4, 6, 9)));
        lists.add(new ArrayList<>(Arrays.asList(6, 9, 10, 100)));

        System.out.println(new KWayMerge().merge(lists, 3));

    }

    class Value implements Comparable<Value>

    {

        int listNum;

        public Value(int listNum, int value) {
            this.listNum = listNum;
            this.value = value;
        }

        int value;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Value value1 = (Value) o;

            if (listNum != value1.listNum) return false;
            return value == value1.value;
        }

        @Override
        public int hashCode() {
            int result = listNum;
            result = 31 * result + value;
            return result;
        }

        @Override
        public int compareTo(Value o) {
            return Integer.compare(this.value, o.value);
        }
    }


    public ArrayList<Integer> merge(ArrayList<ArrayList<Integer>> lists, int k) {

        ArrayList<Integer> res = new ArrayList<>();

        PriorityQueue<Value> heap = new PriorityQueue<>(k);

        ArrayList<Integer> pointers = new ArrayList<>(k);
        for (int i = 0; i < k; i++) {
            pointers.add(0);
        }

        for (int i = 0; i < k; i++) {

            heap.add(new Value(i, lists.get(i).get(0)));
        }

        while (!heap.isEmpty()) {
            Value x = heap.poll();
            int listNum = x.listNum;
            res.add(x.value);
            int listPointer = pointers.get(listNum);
            if (listPointer >= lists.get(listNum).size() - 1)
                continue;

            else {
                pointers.set(listNum, listPointer + 1);
                heap.add(new Value(listNum, lists.get(listNum).get(listPointer + 1)));
            }
        }


        return res;
    }

}
