import java.math.BigInteger;

public class SumRootToLeaf {

    public static void main(String[] args) {

        TreeNode root = new TreeNode(1);
        root.right = new TreeNode(2);

        System.out.println(new SumRootToLeaf().sumNumbers(root));
    }

    public int sumNumbers(TreeNode root) {

        Long[] sum = new Long[1];
        sum[0] = 0l;
        sumHelper(root, new StringBuilder("0"), sum);
        return (int) (sum[0] % 1003);

    }

    public void sumHelper(TreeNode root, StringBuilder num, Long[] sum) {

        if (root == null) return;
        if (root.left == null && root.right == null) {

            num.append(root.val);
            sum[0] += new BigInteger(num.toString()).remainder(new BigInteger("1003")).longValue();
            sum[0] %= 1003;
            num.setLength(num.length() - 1);
            return;
        }


        num.append(root.val);
        sumHelper(root.left, num, sum);
        sumHelper(root.right, num, sum);
        num.setLength(num.length() - 1);
    }
}
