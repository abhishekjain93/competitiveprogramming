public class PermutationRank {


    java.math.BigInteger fact(int n) {
        return (n <= 1) ? java.math.BigInteger.ONE : fact(n - 1).multiply(new java.math.BigInteger(n + ""));
    }

    void populateAndIncreaseCount(int[] count, String str) {
        int i;
        int length = str.length();
        for (i = 0; i < length; ++i)
            ++count[str.charAt(i)];

        for (i = 1; i < 256; ++i)
            count[i] += count[i - 1];
    }

    void updatecount(int[] count, char ch) {
        int i;
        int length = count.length;
        for (i = ch; i < length; ++i)
            --count[i];
    }

    public int findRank(String a) {
        java.math.BigInteger rank = new java.math.BigInteger("1");
        int length = a.length();
        java.math.BigInteger mul = fact(length);
        int[] count = new int[256];
        populateAndIncreaseCount(count, a);

        for (int i = 0; i < length; i++) {

            mul = (mul.divide(new java.math.BigInteger("" + (length - i))));
            rank = rank.add(mul.multiply(new java.math.BigInteger(Integer.toString(count[a.charAt(i)] - 1))));
            updatecount(count, a.charAt(i));
        }
        return rank.remainder(new java.math.BigInteger(1000003 + "")).intValue();


    }
}
