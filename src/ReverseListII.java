public class ReverseListII {

    public static void main(String[] args) {

        ListNode n1 = new ListNode(1);
        ListNode n2 = new ListNode(2);
        ListNode n3 = new ListNode(3);
        ListNode n4 = new ListNode(4);
        ListNode n5 = new ListNode(5);

        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;

        System.out.println(n1);
        System.out.println(new ReverseListII().reverseList(n1, 3, 4));

    }


    public ListNode reverseList(ListNode A, int m, int n) {

        ListNode part1 = A, part2 = A, rev1 = A, rev2 = A;

        int i = 1;
        for (; i < m - 1; i++) {

            part1 = part1.next;
        }

        if (i < m)
            rev1 = part1.next;

        rev2 = rev1;
        for (int j = m; j < n; j++) {

            rev2 = rev2.next;
        }

        part2 = rev2.next;
        rev2.next = null;

        ListNode prev = null, cur = rev1, next = cur.next, c1 = cur, c2 = cur;


        while (cur != null) {

            cur.next = prev;
            prev = cur;
            cur = next;
            if (next != null)
                next = next.next;

        }
        c2 = prev;

        if (part1 != rev1)
            part1.next = c2;
        c1.next = part2;

        if (part1 != rev1)
            return A;
        else
            return rev2;
    }

}
