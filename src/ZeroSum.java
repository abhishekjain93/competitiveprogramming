import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ZeroSum {
	public ArrayList<Integer> lszero(ArrayList<Integer> a) {

		int size = a.size();
		int sum = 0;
		int maxstart = 0, curstart = 0;
		int maxlen = 0, curlen = 0;
		HashMap<Integer, Integer> map = new HashMap<>();

		for (int i = 0; i < size; i++) {

			sum += a.get(i);
			if (sum == 0) {

				curlen = i + 1;
				curstart = -1;

			} else if (map.containsKey(sum)) {

				curstart = map.get(sum);
				curlen = i - curstart;

			} else {

				map.put(sum, i);
			}

			if (curlen > maxlen) {
				maxlen = curlen;
				maxstart = curstart;

			}
		}

		return new ArrayList<Integer>(a.subList(maxstart+1, maxstart + maxlen+1));
	}

	public static void main(String[] args) {
		System.out.println(new ZeroSum().lszero(new ArrayList<>(Arrays.asList(1, 2, -3, 3))));
	}
}
