public class MergeSortLinkedList {


    public static void main(String[] args) {
        ListNode n1 = new ListNode(2);
        ListNode n2 = new ListNode(4);
        ListNode n3 = new ListNode(1);
        ListNode n4 = new ListNode(3);
        ListNode n5 = new ListNode(6);

        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;
        System.out.println(n1);
        System.out.println(new MergeSortLinkedList().sortList(n1));


    }


    public ListNode sortList(ListNode A) {

        if (A == null || A.next == null)
            return A;


        ListNode preMid = findMiddle(A);
        ListNode mid = preMid.next;

        preMid.next = null;
        ListNode l1 = sortList(A);
        ListNode l2 = sortList(mid);

        // step 3. merge l1 and l2
        return merge(l1, l2);


    }

    private ListNode merge(ListNode l1, ListNode l2) {

        ListNode l = new ListNode(0), p = l;

        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                p.next = l1;
                l1 = l1.next;
            } else {
                p.next = l2;
                l2 = l2.next;
            }
            p = p.next;
        }

        if (l1 != null)
            p.next = l1;

        if (l2 != null)
            p.next = l2;

        return l.next;
    }

    private ListNode findMiddle(ListNode h) {
        if (h == null)
            return h;
        ListNode fastptr = h.next;
        ListNode slowptr = h;

        // Move fastptr by two and slow ptr by one
        // Finally slowptr will point to middle node
        while (fastptr != null) {
            fastptr = fastptr.next;
            if (fastptr != null) {
                slowptr = slowptr.next;
                fastptr = fastptr.next;
            }
        }
        return slowptr;

    }


}
