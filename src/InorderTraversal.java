import java.util.ArrayList;

public class InorderTraversal {

    public ArrayList<Integer> inorderTraversal(TreeNode root) {

        ArrayList<Integer> res = new ArrayList<>();
        traversalHelper(root, res);
        return res;
    }


    private void traversalHelper(TreeNode root, ArrayList<Integer> res) {

        if (root == null) return;
        traversalHelper(root.left, res);
        res.add(root.val);
        traversalHelper(root.right, res);

    }
}
