import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Successive1 {
	public ArrayList<Integer> maxone(ArrayList<Integer> a, int b) {

		int l = 0;
		int r = 0;
		int size = a.size();
		int numzeros = 0;
		int rStart = 0;
		int rEnd = 0;
		ArrayList<Integer> zeroes = new ArrayList<Integer>();
		while (r <= size - 1) {

			if (numzeros > b) {
				while (numzeros > b) {
					if (a.get(l) == 0) {
						numzeros--;
					}
					l++;
				}
			}
			r++;
			if (a.get(r - 1) == 0) {
				numzeros++;
				if (numzeros > b) {
					continue;
				}
			}
			if (r - l > rEnd - rStart) {
				rStart = l;
				rEnd = r;
			}
		}
		// List<Integer> result = a.subList(rStart, rEnd);
		for (int i = rStart; i < rEnd; i++) {

			zeroes.add(i);

		}

		return zeroes;
	}

	public static void main(String[] args) {

		//System.out.println(new Integer(1) == (int) new Integer(1));
		//System.out.println(new Successive1()
		//		.maxone(new ArrayList<>(Arrays.asList(1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0)), 4));

			System.out.println(rev("Amsterdam", 0, 4));
	}

	public static String rev(String s, int l, int n) {

		if (l == n)
			return ""+s.charAt(l);
		return s.charAt(n) + rev(s, l, n - 1);

	}

}
