import java.util.HashMap;

public class RomanToInteger {


    public int romanToInt(String roman) {

        HashMap<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);

        int len = roman.length();
        int sum = 0;

        if (len == 1)
            return map.get(roman.charAt(0));

        for (int i = 0; i < len - 1; i++) {

            if (map.get(roman.charAt(i)) < map.get(roman.charAt(i + 1))) {

                sum += map.get(roman.charAt(i + 1)) - map.get(roman.charAt(i));
                i++;

            } else {

                sum += map.get(roman.charAt(i));

            }
        }

        if (map.get(roman.charAt(len - 2)) >= map.get(roman.charAt(len - 1))) {

            sum += map.get(roman.charAt(len - 1));
        }
        return sum;
    }

    public static void main(String[] args) {

        System.out.println(new RomanToInteger().romanToInt("V"));

    }
}
