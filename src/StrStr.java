public class StrStr {

    public int strStr(String text, String pattern) {

        int[] lps = getLps(pattern);
        int size = text.length();
        int pSize = pattern.length();
        int i = 0, j = 0;

        while (i + j < size) {

            if (pattern.charAt(j) == text.charAt(i + j)) {

                j++;
                if (j == pSize) return i;

            } else {


                if (j > 0) {

                    i += j - lps[j - 1];
                    j = lps[j - 1];
                } else {

                    i += 1;
                }

            }
        }


        return -1;
    }

    private int[] getLps(String pattern) {

        int[] lps = new int[pattern.length()];
        lps[0] = 0;

        int len = 0;

        for (int i = 1; i < lps.length; ) {

            if (pattern.charAt(i) == pattern.charAt(len))
                lps[i++] = ++len;

            else {

                if (len == 0)
                    lps[i++] = 0;

                else {
                    len = lps[len - 1];
                }
            }
        }
        return lps;
    }

    public static void main(String[] args) {

        System.out.println(new StrStr().strStr("bbaabbbbbaabbaabbbbbbabbbabaabbbabbabbbbababbbabbabaaababbbaabaaaba", "babaaa"));
    }
}
