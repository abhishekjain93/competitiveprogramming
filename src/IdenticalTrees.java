public class IdenticalTrees {

    public int isSameTree(TreeNode a, TreeNode b) {

        if (a == b) return 1;

        if (a != null && b != null && a.val == b.val)
            return Math.min(isSameTree(a.left, b.left), isSameTree(a.right, b
                    .right));

        return 0;
    }

}
