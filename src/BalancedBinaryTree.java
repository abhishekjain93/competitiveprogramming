public class BalancedBinaryTree {

    public int isBalanced(TreeNode root) {

        return isBalancedHelper(root) == -1 ? 0 : 1;
    }

    public int isBalancedHelper(TreeNode root) {

        if (root == null) return 0;
        int hLeft = isBalancedHelper(root.left);
        int hRight = isBalancedHelper(root.right);

        if (hLeft == -1 || hRight == -1) return -1;

        if (Math.abs(hLeft - hRight) > 1) return -1;
        return Math.max(hLeft, hRight) + 1;

    }

}
