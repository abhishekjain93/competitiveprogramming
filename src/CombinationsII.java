import java.util.*;

public class CombinationsII {


    public static void main(String[] args) {

        System.out.println(new CombinationsII().combinationSum(new ArrayList<>(Arrays.<Integer>asList(10, 1, 2, 7, 6, 5, 1)), 8));

    }

    public ArrayList<ArrayList<Integer>> combinationSum(ArrayList<Integer> list, int sum) {

        Set<ArrayList<Integer>> res = new HashSet<>();
        Collections.sort(list);
        combinationSum(list, sum, 0, new ArrayList<>(), res);

        return new ArrayList<>(res);
    }

    public void combinationSum(ArrayList<Integer> list, int sum, int start, ArrayList<Integer> path, Set<ArrayList<Integer>> res) {

        if (sum == 0) {
            res.add(new ArrayList<>(path));
            return;
        }
        if (sum < 0 || start >= list.size()) return;

        path.add(list.get(start));
        combinationSum(list, sum - list.get(start), start + 1, path, res);
        path.remove(path.size() - 1);
        combinationSum(list, sum, start + 1, path, res);

    }
}
