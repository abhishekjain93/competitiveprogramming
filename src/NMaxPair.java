import java.util.*;

public class NMaxPair {

    public static void main(String[] args) {

        System.out.println(new NMaxPair().solve(new ArrayList<>(Arrays.asList(1, 4, 2, 3)), new ArrayList<>(Arrays.asList(2, 5, 1, 6))));

    }


    class ArrayOffset {
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ArrayOffset that = (ArrayOffset) o;

            if (i != that.i) return false;
            if (j != that.j) return false;
            return val == that.val;
        }

        @Override
        public int hashCode() {
            int result = i;
            result = 31 * result + j;
            result = 31 * result + val;
            return result;
        }

        public ArrayOffset(int i, int j, int val) {
            this.i = i;
            this.j = j;
            this.val = val;
        }

        int i;
        int j;
        int val;
    }

    public ArrayList<Integer> solve(ArrayList<Integer> list1, ArrayList<Integer> list2) {

        Collections.sort(list1);
        Collections.sort(list2);
        ArrayList<Integer> res = new ArrayList<>();
        int size = list1.size();

        PriorityQueue<ArrayOffset> heap = new PriorityQueue<>(new Comparator<ArrayOffset>() {
            @Override
            public int compare(ArrayOffset o1, ArrayOffset o2) {
                return Integer.compare(o2.val, o1.val);
            }
        });

        HashSet<ArrayOffset> set = new HashSet<>();

        ArrayOffset e = new ArrayOffset(list1.size() - 1, list2.size() - 1, list1.get(list1.size() - 1) + list2.get(list2.size() - 1));
        heap.add(e);
        set.add(e);

        for (int i = 1; i <= size; i++) {

            ArrayOffset poll = heap.poll();
            res.add(poll.val);

            if (poll.j >= 1) {
                ArrayOffset e1 = new ArrayOffset(poll.i, poll.j - 1, list1.get(poll.i) + list2.get(poll.j - 1));

                if (!set.contains(e1)) {
                    heap.add(e1);
                    set.add(e1);
                }
            }

            if (poll.i >= 1) {
                ArrayOffset e2 = new ArrayOffset(poll.i - 1, poll.j, list1.get(poll.i - 1) + list2.get(poll.j));

                if (!set.contains(e2)) {
                    heap.add(e2);
                    set.add(e2);
                }
            }

        }
        return res;
    }

}
