import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PreorderInorder {

    private static int preIndex = 0;

    public static void main(String[] args) {

        System.out.println(new PreorderInorder().buildTree(new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5)), new ArrayList<>
                (Arrays.asList(3, 2, 4, 1, 5))));

    }

    public TreeNode buildTree(ArrayList<Integer> preorder, ArrayList<Integer> inorder) {

        if (inorder.isEmpty() || preorder.isEmpty()) return null;

        return makeTreeUtil(inorder, preorder, 0, inorder.size() - 1);

    }

    private TreeNode makeTreeUtil(List<Integer> inorder, List<Integer> preorder, int inStart, int inEnd) {

        if (inStart > inEnd) return null;

        int nodeVal = preorder.get(preIndex++);
        TreeNode node = new TreeNode(nodeVal);

        if (inStart == inEnd) return node;

        final int nodeIndex = inorder.indexOf(nodeVal);
        node.left = makeTreeUtil(inorder, preorder, inStart, nodeIndex - 1);

        node.right = makeTreeUtil(inorder, preorder, nodeIndex + 1, inEnd);

        return node;

    }

}
