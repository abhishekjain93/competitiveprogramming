import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LongestArithmeticSequence {

    public static void main(String[] args) {

        System.out.println(new LongestArithmeticSequence().longestArithmeticSequence(Arrays.asList(8, 10, 6)));

    }

    public int longestArithmeticSequence(final List<Integer> list) {

        Collections.sort(list);
        int n = list.size();
        if (list.size() < 2) return 0;
        if (list.size() < 3) return 2;

        int[][] dp = new int[n][n];

        for (int i = 0; i < n; i++) {
            dp[i][n - 1] = 2;

        }

        int maxLenSequence = 0;

        for (int j = n - 2; j > 0; j--) {

            int i = j - 1, k = j + 1;

            while (i >= 0 && k <= n - 1) {

                if (list.get(i) + list.get(k) < 2 * list.get(j)) {
                    k++;

                } else if (list.get(i) + list.get(k) > 2 * list.get(j)) {

                    dp[i][j] = 2;
                    i--;

                } else {

                    dp[i][j] = dp[j][k] + 1;
                    maxLenSequence = Math.max(maxLenSequence, dp[i][j]);
                    i--;
                    k++;

                }

            }
            while (i >= 0) {
                dp[i][j] = 2;
                i--;
            }
        }

        return maxLenSequence;
    }

}
