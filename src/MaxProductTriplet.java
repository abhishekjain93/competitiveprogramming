import java.util.ArrayList;

public class MaxProductTriplet {

    public int maxp3(ArrayList<Integer> list) {

        int min = Integer.MAX_VALUE, secondMin = Integer.MAX_VALUE, max = Integer.MIN_VALUE, secondMax = Integer
                .MIN_VALUE,
                thirdMax = Integer.MIN_VALUE;

        for (int x : list) {

            if (x < secondMin) {

                if (x < min) {

                    secondMin = min;
                    min = x;

                } else {
                    secondMin = x;
                }
            }
            if (x > thirdMax) {

                if (x > secondMax) {

                    if (x > max) {

                        thirdMax = secondMax;
                        secondMax = max;
                        max = x;

                    } else {

                        thirdMax = secondMax;
                        secondMax = x;

                    }

                } else {

                    thirdMax = x;

                }
            }
        }

        return Math.max(max * secondMax * thirdMax, max * min * secondMin);
    }

}
