public class DungeonPrincessV3 {

    public int calculateMinimumHP(int[][] dungeon) {
        int rows = dungeon.length;
        int cols = rows == 0 ? 0 : dungeon[0].length;
        int[][] minRequred = new int[rows][cols];

        minRequred[rows - 1][cols - 1] = Math.max(1, 1 - dungeon[rows - 1][cols - 1]);

        for (int i = rows - 2; i >= 0; i--) {
            minRequred[i][cols - 1] = Math.max(1, minRequred[i + 1][cols - 1] - dungeon[i][cols - 1]);
        }
        for (int j = cols - 2; j >= 0; j--) {
            minRequred[rows - 1][j] = Math.max(1, minRequred[rows - 1][j + 1] - dungeon[rows - 1][j]);
        }

        for (int i = rows - 2; i >= 0; i--) {
            for (int j = cols - 2; j >= 0; j--) {
                minRequred[i][j] = Math.max(1, Math.min(minRequred[i + 1][j], minRequred[i][j + 1]) - dungeon[i][j]);
            }
        }

        return minRequred[0][0];
    }
}
