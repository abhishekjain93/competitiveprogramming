public class EvaluateToTrue {

    public static void main(String[] args) {

        System.out.println(new EvaluateToTrue().cnttrue("T^T^T^F|F&F^F|T^F^T"));

    }

    public int cnttrue(String exp) {

        int n = exp.length();
        char[] operands = new char[(n + 1) / 2];
        char[] operators = new char[(n - 1) / 2];

        for (int i = 0, j = 0; i < n; i += 2, j++) {
            operands[j] = exp.charAt(i);
        }

        for (int j = 1, i = 0; j < n; j += 2, i++) {
            operators[i] = exp.charAt(j);
        }

        int T[][] = new int[(n + 1) / 2][(n + 1) / 2];
        int F[][] = new int[(n + 1) / 2][(n + 1) / 2];

        int l = (n + 1) / 2;

        for (int i = 0; i < l; i++) {

            T[i][i] = operands[i] == 'T' ? 1 : 0;
            F[i][i] = operands[i] == 'F' ? 1 : 0;
        }

        for (int g = 1; g < l; g++) {

            for (int i = 0; i + g < l; i++) {
                int j = i + g;
                T[i][j] = 0;
                F[i][j] = 0;

                for (int k = i; k < j; k++) {
                    int tik = T[i][k] + F[i][k];
                    int tkj = T[k + 1][j] + F[k + 1][j];
                    if (operators[k] == '&') {

                        T[i][j] += T[i][k] * T[k + 1][j];
                        F[i][j] += ((tik * tkj) - T[i][k] * T[k + 1][j]);

                    }
                    if (operators[k] == '|') {

                        T[i][j] += ((tik * tkj) - F[i][k] * F[k + 1][j]);
                        F[i][j] += F[i][k] * F[k + 1][j];

                    }
                    if (operators[k] == '^') {

                        T[i][j] += F[i][k] * T[k + 1][j] + T[i][k] * F[k + 1][j];
                        F[i][j] += T[i][k] * T[k + 1][j] + F[i][k] * F[k + 1][j];

                    }

                }

            }

        }
        return T[0][l - 1]%1003;
    }

}
