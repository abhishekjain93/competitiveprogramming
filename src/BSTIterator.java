import java.util.Stack;

public class BSTIterator {

    TreeNode root;
    Stack<TreeNode> stack = new Stack<>();

    public BSTIterator(TreeNode root) {

        this.root = root;
        TreeNode node = root;
        while (node != null) {
            stack.push(node);
            node = node.left;
        }
    }

    public int next() {

        TreeNode next = stack.pop();
        TreeNode node = next.right;
        while (node != null) {
            stack.push(node);
            node = node.left;
        }
        return next.val;
    }

    public boolean hasNext() {

        return !stack.empty();
    }
}
