import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class DistinctWindow {

    public static void main(String[] args) {

        System.out.println(new DistinctWindow().dNums(new ArrayList<Integer>(Arrays.asList(1, 2, 1, 3, 4, 3)), 3));

    }

    public ArrayList<Integer> dNums(ArrayList<Integer> list, int k) {

        ArrayList<Integer> res = new ArrayList<>();
        HashMap<Integer, Integer> map = new HashMap<>();
        int distinct = 0;

        for (int i = 0; i < k; i++) {

            if (map.get(list.get(i)) != null) {

                map.put(list.get(i), map.get(list.get(i)) + 1);

            } else {

                map.put(list.get(i), 1);
                distinct++;
            }
        }
        res.add(distinct);

        for (int i = 1; i + k - 1 < list.size(); i++) {

            if (map.get(list.get(i - 1)) == 1) {

                distinct--;
                map.remove(list.get(i - 1));

            } else
                map.put(list.get(i - 1), map.get(list.get(i - 1)) - 1);


            if (map.get(list.get(i + k - 1)) != null) {

                map.put(list.get(i + k - 1), map.get(list.get(i + k - 1)) + 1);

            } else {

                map.put(list.get(i + k - 1), 1);
                distinct++;
            }
            res.add(distinct);
        }

        return res;
    }

}
