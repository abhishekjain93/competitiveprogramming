public class NextRightPointer {

    public void connect(TreeLinkNode root) {

        TreeLinkNode currLevelPtr = root;
        TreeLinkNode currLevelPtr2;
        TreeLinkNode nextLevelStart = null;

        while (currLevelPtr != null) {

            if (nextLevelStart == null) {

                nextLevelStart = currLevelPtr.left != null ? currLevelPtr.left : currLevelPtr.right;
            }

            if (currLevelPtr.left != null && currLevelPtr.right != null) {

                currLevelPtr.left.next = currLevelPtr.right;

            }

            currLevelPtr2 = currLevelPtr.next;

            while (currLevelPtr2 != null && currLevelPtr2.left == null && currLevelPtr2.right == null) {

                currLevelPtr2 = currLevelPtr2.next;
            }

            if (currLevelPtr2 != null) {

                if (currLevelPtr.left != null || currLevelPtr.right != null) {

                    if (currLevelPtr.right != null) {

                        currLevelPtr.right.next = currLevelPtr2.left != null ? currLevelPtr2.left : currLevelPtr2.right;

                    } else {

                        currLevelPtr.left.next = currLevelPtr2.left != null ? currLevelPtr2.left : currLevelPtr2.right;
                    }
                }
                currLevelPtr = currLevelPtr2;

            } else {

                currLevelPtr = nextLevelStart;
                nextLevelStart = null;

            }

        }

    }

}

class TreeLinkNode {

    TreeLinkNode left;
    TreeLinkNode right;
    TreeLinkNode next;

}