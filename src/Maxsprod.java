import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class Maxsprod {

    public static void main(String[] args) {

        System.out.println(new Maxsprod().maxSpecialProduct(new ArrayList<>(Arrays.asList(5, 9, 6, 8, 6, 4, 6, 9, 5, 4, 9))));

    }

    public int maxSpecialProduct(ArrayList<Integer> list) {

        int size = list.size();
        int[] leftValues = new int[size];
        int[] rightValues = new int[size];

        Stack<Pair> stack = new Stack<>();
        stack.push(new Pair(0, list.get(0)));

        for (int i = 1; i < list.size(); i++) {

            if (stack.isEmpty() || list.get(i) <= stack.peek().y) {
                stack.push(new Pair(i, list.get(i)));

            } else {

                while (!stack.isEmpty() && stack.peek().y < list.get(i)) {

                    ArrayList<Integer> indexes = new ArrayList<>();
                    Pair pop = stack.pop();
                    int x = pop.x;
                    indexes.add(x);
                    while (!stack.isEmpty() && stack.peek().y == pop.y) {
                        pop = stack.pop();
                        indexes.add(pop.x);
                    }

                    for (int idx : indexes) {

                        leftValues[idx] = stack.isEmpty() ? 0 : stack.peek().x;
                    }
                }
                stack.push(new Pair(i, list.get(i)));
            }
        }

        while (!stack.isEmpty()) {

            ArrayList<Integer> indexes = new ArrayList<>();
            Pair pop = stack.pop();
            int x = pop.x;
            indexes.add(x);
            while (!stack.isEmpty() && stack.peek().y == pop.y) {
                pop = stack.pop();
                indexes.add(pop.x);
            }

            for (int idx : indexes) {

                leftValues[idx] = stack.isEmpty() ? 0 : stack.peek().x;
            }

        }

        stack.push(new Pair(size - 1, list.get(size - 1)));

        for (int i = size - 2; i >= 0; i--) {

            if (stack.isEmpty() || list.get(i) <= stack.peek().y) {
                stack.push(new Pair(i, list.get(i)));

            } else {

                while (!stack.isEmpty() && stack.peek().y < list.get(i)) {

                    ArrayList<Integer> indexes = new ArrayList<>();
                    Pair pop = stack.pop();
                    int x = pop.x;
                    indexes.add(x);
                    while (!stack.isEmpty() && stack.peek().y == pop.y) {
                        pop = stack.pop();
                        indexes.add(pop.x);
                    }

                    for (int idx : indexes) {

                        rightValues[idx] = stack.isEmpty() ? 0 : stack.peek().x;
                    }

                }
                stack.push(new Pair(i, list.get(i)));
            }
        }

        while (!stack.isEmpty()) {

            ArrayList<Integer> indexes = new ArrayList<>();
            Pair pop = stack.pop();
            int x = pop.x;
            indexes.add(x);
            while (!stack.isEmpty() && stack.peek().y == pop.y) {
                pop = stack.pop();
                indexes.add(pop.x);
            }

            for (int idx : indexes) {

                rightValues[idx] = stack.isEmpty() ? 0 : stack.peek().x;
            }

        }

        int max = 0;

        for (int i = 0; i < size; i++) {

            max = Math.max(max, ((leftValues[i] % 1000000007) * (rightValues[i] % 1000000007)) % 1000000007);

        }


       /* System.out.println(list);
        System.out.println(Arrays.toString(leftValues));
        System.out.println(Arrays.toString(rightValues));
*/
        return max;
    }

    class Pair {
        int x = 0;
        int y = 0;

        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

}
