public class InsertionSortList {


    public static void main(String[] args) {
        ListNode n1 = new ListNode(80);
        ListNode n2 = new ListNode(14);
        ListNode n3 = new ListNode(12);
        ListNode n4 = new ListNode(97);
        ListNode n5 = new ListNode(5);
        ListNode n6 = new ListNode(6);
        ListNode n7 = new ListNode(7);

        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
      /*  n4.next = n5;
        n5.next = n6;
        n6.next = n7;
      */  System.out.println(n1);
        System.out.println(new InsertionSortList().insertionSortList(n1));

    }

    public ListNode insertionSortList(ListNode A) {

        ListNode sortStart = A, sortEnd = A;

        while (sortEnd.next != null) {

            ListNode e = sortEnd.next;
            ListNode i = sortStart;
            ListNode iPrev = A;
            sortEnd.next = e.next;
            while (i != sortEnd) {

                if (e.val < i.val) {

                    if (i == sortStart) {

                        A = sortStart = e;
                        e.next = i;

                        break;
                    }

                    e.next = i;
                    if (iPrev != null)
                        iPrev.next = e;
                    break;
                }
                iPrev = i;
                i = i.next;

            }
            if (i == sortEnd) {

                if (e.val < i.val) {

                    if (i == sortStart) {

                        A = sortStart = e;
                        e.next = i;

                        continue;
                    }

                    e.next = i;
                    if (iPrev != null)
                        iPrev.next = e;
                } else {
                    e.next = i.next;
                    i.next = e;
                    sortEnd = e;
                }
            }
        }

        return A;
    }
}


