import java.util.ArrayList;
import java.util.Arrays;

public class Flip {

	public ArrayList<Integer> flip(String bstr) {

		int L = 0, R = 0, currentL = 0, currentR = -1;
		boolean hzero = false;
		int maxCount = 0, zeroCount = 0, length = bstr.length();

		for (int i = 0; i < length; i++) {

			currentR++;
			if (bstr.charAt(i) == '0') {
				hzero = true;
				zeroCount++;
				if (zeroCount > maxCount) {

					maxCount = zeroCount;
					L = currentL;
					R = currentR;
				}

			} else {

				zeroCount--;
				if (zeroCount < 0) {

					zeroCount = 0;
					currentL = i + 1;
					currentR = i;
				}
			}
		}
		if (zeroCount > maxCount) {
			maxCount = zeroCount;
			L = currentL;

		}

		if (!hzero)
			return new ArrayList<>();
		return new ArrayList<>(Arrays.asList(L + 1, R + 1));

	}

	public static void main(String[] args) {

		System.out.println(new Flip().flip("010"));
	}

}
