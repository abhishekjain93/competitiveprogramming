import java.util.*;

public class VerticalOrderTraversal {

    class OffsetTreeNode {

        public TreeNode node;

        public OffsetTreeNode(TreeNode node, int offset) {
            this.node = node;
            this.offset = offset;
        }

        public int offset;

    }

    public ArrayList<ArrayList<Integer>> verticalOrderTraversal(TreeNode root) {

        Map<Integer, ArrayList<Integer>> map = new HashMap<>();
        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        Queue<OffsetTreeNode> queue = new LinkedList<>();

        queue.add(new OffsetTreeNode(root, 0));

        while (!queue.isEmpty()) {

            OffsetTreeNode ofsNode = queue.poll();
            int offset = ofsNode.offset;
            TreeNode node = ofsNode.node;

            min = Math.min(min, offset);
            max = Math.max(max, offset);

            if (map.get(offset) == null) {
                map.put(offset, new ArrayList<>());
            }

            map.get(offset).add(node.val);

            if (node.left != null) queue.add(new OffsetTreeNode(node.left, offset - 1));
            if (node.right != null) queue.add(new OffsetTreeNode(node.right, offset + 1));
        }

        ArrayList<ArrayList<Integer>> res = new ArrayList<>();

        for (int i = min; i <= max; i++) {

            res.add(map.get(i));
        }

        return res;
    }
}
