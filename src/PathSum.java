public class PathSum {

    public int hasPathSum(TreeNode root, int sum) {

        if (root == null) return 0;

        if (root.left == null && root.right == null)
            if (sum - root.val == 0) return 1;
            else return 0;

        return hasPathSum(root.left, sum - root.val) | hasPathSum(root.right, sum - root.val);

    }

}
