import java.util.ArrayList;
import java.util.Arrays;

public class Permutations {

    public ArrayList<ArrayList<Integer>> permute(ArrayList<Integer> a) {

        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        res.add(new ArrayList<>(Arrays.asList(a.get(0))));
        int length = a.size();
        for (int i = 1; i < length; i++) {

            res = merge(a.get(i), res);

        }

        return res;
    }

    ArrayList<ArrayList<Integer>> merge(Integer x, ArrayList<ArrayList<Integer>> list) {

        ArrayList<ArrayList<Integer>> newStrings = new ArrayList<>();

        for (ArrayList<Integer> str : list) {
            int length = str.size();

            for (int i = 0; i <= length; i++) {

                ArrayList<Integer> newStr = new ArrayList<>();
                newStr.addAll(str.subList(0, i));
                newStr.add(x);
                newStr.addAll(str.subList(i, length));
                newStrings.add(newStr);
            }

        }
        list = newStrings;
        return list;
    }

    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> permutations = new Permutations().permute(new ArrayList<>(Arrays.asList(1,2)));
        System.out.println(permutations);
        System.out.println(permutations.size());

    }
}
