import java.util.LinkedList;
import java.util.Queue;

public class InterleavingStrings {

    class Pair {

        int p1;
        int p2;

        public Pair(int i1, int i2) {

            p1 = i1;
            p2 = i2;
        }

    }

    public int isInterleave(String s1, String s2, String s3) {

        int len1 = s1.length();
        int len2 = s2.length();
        int len3 = s3.length();

        if (len1 + len2 != len3) return 0;

        Queue<Pair> queue = new LinkedList<>();

        if (s1.charAt(0) == s3.charAt(0))
            queue.add(new Pair(0, -1));

        else if (s2.charAt(0) == s3.charAt(0))
            queue.add(new Pair(-1, 0));

        else return 0;

        while (!queue.isEmpty()) {

            Pair sol = queue.poll();
            if (sol.p1 + sol.p2 == len3 - 2) return 1;
            if (sol.p1 + sol.p2 > len3 - 2) continue;

            if (sol.p1 < len1 - 1 && s1.charAt(sol.p1 + 1) == s3.charAt(sol.p1 + sol.p2 + 2))
                queue.add(new Pair(sol.p1 + 1, sol.p2));

            if (sol.p2 < len2 - 1 && s2.charAt(sol.p2 + 1) == s3.charAt(sol.p1 + sol.p2 + 2))
                queue.add(new Pair(sol.p1, sol.p2 + 1));

        }

        return 0;
    }

    public static void main(String[] args) {

        System.out.println(new InterleavingStrings().isInterleave("abc", "def", "abcdef"));

    }
}
