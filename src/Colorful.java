import java.util.HashSet;
import java.util.Set;

public class Colorful {

	public int colorful(int a) {

		String num = "" + a;

		int length = num.length();
		if (length > 8)
			return 0;
		int[] digit = new int[length];

		for (int i = 0; i < length; i++) {
			digit[i] = num.charAt(i) - '0';
		}

		if (length == 1)
			return 1;
		if (length == 2 && (digit[0] == 0 || digit[0] == 1 || digit[1] == 0 || digit[1] == 1))
			return 0;

		Set<Integer> products = new HashSet<>();

		for (int i = 0; i < length; i++) {
			int product = 1;
			for (int j = 1; j + i - 1 < length; j++) {

				product = 1;
				for (int k = i; k < i + j; k++)
					product *= digit[k];

				if (products.contains(product))
					return 0;
				products.add(product);
			}

		}
		return 1;
	}

	public static void main(String[] args) {

		System.out.println(new Colorful().colorful(123));

	}
}