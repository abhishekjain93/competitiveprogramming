public class RotateList {

    public ListNode rotateList(ListNode A, int x) {

        ListNode p = A, q = A, r = A;
        int len = 0;

        while (r != null) {

            len++;
            r = r.next;
        }

        if (x > len)
            x = x % len;

        if (x == 0) return A;

        for (int i = 0; i < x; i++) {

            p = p.next;

        }
        if (p == null) return A;
        while (p.next != null) {

            p = p.next;
            q = q.next;
        }

        ListNode returnList = q.next;
        q.next = null;
        p.next = A;
        return returnList;
    }


    public int findRank(String a) {
        java.math.BigInteger rank = new java.math.BigInteger("1");
        int length = a.length();
        java.math.BigInteger mul = fact(length);
        int[] count = new int[256];
        for (int i = 0; i < length; i++) {

            mul = (mul.divide(new java.math.BigInteger("" + (length - i))));
            rank = rank.add(mul.multiply(new java.math.BigInteger(Integer.toString(count[a.charAt(i)] - 1))));

        }
        return rank.remainder(new java.math.BigInteger(1000003 + "")).intValue();


    }

    java.math.BigInteger fact(int n) {
        return (n <= 1) ? java.math.BigInteger.ONE : fact(n - 1).multiply(new java.math.BigInteger(n + ""));
    }

}
