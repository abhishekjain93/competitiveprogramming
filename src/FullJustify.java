import java.util.ArrayList;
import java.util.Arrays;

public class FullJustify {
	public ArrayList<String> fullJustify(ArrayList<String> a, int b) {
		ArrayList<String> res = new ArrayList<>();
		int size = a.size();

		int low = 0;
		int high = 0;
		while (high <= size - 1) {
			int counter = 0;

			while (counter < b && high <= size - 1) {
				counter += a.get(high).length();
				high++;
			}
			high--;
			if (counter > b) {
				counter -= a.get(high).length();
				high--;
			}
			if (b - counter < high - low) {

				while (b - counter < high - low) {

					counter -= a.get(high).length();
					high--;
				}
			}

			if (high == size - 1) { // last line

				StringBuilder strlast = new StringBuilder(50);

				for (int l = low; l <= high; l++) {

					strlast.append(a.get(l)).append(' ');

				}
				if (strlast.length() > b)
					strlast.deleteCharAt(strlast.length() - 1);

				while (strlast.length() != b)
					strlast.append(' ');

				res.add(strlast.toString());
				break;

			}
			int numBlocks = (high == low ? 1 : high - low);
			int equalspace = (b - counter) / numBlocks;
			int extraspace = (b - counter) % numBlocks;
			StringBuilder str = new StringBuilder(50);
			str.append(a.get(low));

			if (high == low) {
				for (int x = 0; x < equalspace; x++)
					str.append(' ');

			} else {
				while (numBlocks > 0) {
					for (int x = 0; x < equalspace; x++)
						str.append(' ');
					if (extraspace > 0) {
						str.append(' ');
						extraspace--;
					}
					str.append(a.get(++low));
					numBlocks--;
				}
			}
			res.add(str.toString());
			low = high + 1;
			high = low;
		}

		return res;
	}

	public static void main(String[] args) {

		ArrayList<String> list = new ArrayList<>(Arrays.asList("am", "fasgoprn", "lvqsrjylg", "rzuslwan", "xlaui",
				"tnzegzuzn", "kuiwdc", "fofjkkkm", "ssqjig", "tcmejefj", "uixgzm", "lyuxeaxsg", "iqiyip", "msv",
				"uurcazjc", "earsrvrq", "qlq", "lxrtzkjpg", "jkxymjus", "mvornwza", "zty", "q", "nsecqphjy"));

		System.out.println(new FullJustify().fullJustify(list, 14));

	}

}