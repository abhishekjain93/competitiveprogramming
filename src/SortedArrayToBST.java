import java.util.List;

public class SortedArrayToBST {


    public TreeNode sortedArrayToBST(final List<Integer> a) {

        int size = a.size();
        if (size == 0) return null;
        if (size == 1) return new TreeNode(a.get(0));

        TreeNode root = new TreeNode(a.get(size / 2));
        root.left = sortedArrayToBST(a.subList(0, size / 2));
        root.right = sortedArrayToBST(a.subList(size / 2 + 1, size));

        return root;
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    @Override
    public String toString() {
        return "TreeNode{" +
                "val=" + val +
                '}';
    }

    TreeNode(int x) {
        val = x;
    }
}
