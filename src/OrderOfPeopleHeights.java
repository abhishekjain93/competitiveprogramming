import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

class Person {

    int height;
    int front;

    public Person(int height, int front) {
        this.height = height;
        this.front = front;
    }
}

public class OrderOfPeopleHeights {

    public ArrayList<Integer> order(ArrayList<Integer> heights, ArrayList<Integer> pFront) {

        Person[] persons = new Person[heights.size()];

        for (int i = 0; i < heights.size(); i++) {

            persons[i] = new Person(heights.get(i), pFront.get(i));
        }

        Arrays.sort(persons, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {

                if (o1.height != o2.height)
                    return o2.height - o1.height;

                return o1.front - o2.front;
            }
        });

        ArrayList<Integer> res = new ArrayList<>();
        for (int i = 0; i < heights.size(); i++) {

            res.add(persons[i].front, persons[i].height);
        }

        return res;
    }


}
