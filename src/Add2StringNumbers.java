public class Add2StringNumbers {

    static String sum(String a, String b) {

        int lenA = a.length();
        int lenB = b.length();
        int carry = 0;
        int ans = 0;
        int base = 1;
        int i = lenA - 1;
        int j = lenB - 1;

        int digitA = 0;
        int digitB = 0;

        while (i >= 0 || j >= 0) {

            if (i >= 0)
                digitA = a.charAt(i) - '0';
            else
                digitA = 0;

            if (j >= 0)
                digitB = b.charAt(j) - '0';
            else
                digitB = 0;

            int digitSum = digitA + digitB + carry;
            if (digitSum > 9) {

                digitSum = digitSum - 10;
                carry = 1;

            } else {
                carry = 0;
            }
            ans = ans + base * (digitSum);
            base = base * 10;
            i--;
            j--;
        }

        if (carry == 1) {

            ans += base;

        }

        return Integer.toString(ans);
    }
}

