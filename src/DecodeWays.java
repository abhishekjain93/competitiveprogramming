public class DecodeWays {

    public int numDecodings(String code) {

        int[] memo = new int[code.length() + 1];

        for (int i = 0; i < code.length(); i++) {

            memo[i] = -1;
        }
        memo[code.length()] = 1;
        return decodeWays(code, 0, memo);

    }

    private int decodeWays(String code, int index, int[] memo) {


        if (memo[index] != -1) return memo[index];

        if (code.charAt(index) == '0') return memo[index] = 0;

        int ans = decodeWays(code, index + 1, memo);

        if (index < code.length() - 1 && (code.charAt(index) == '1' || (code.charAt(index) == '2' && code.charAt
                (index + 1)
                < '7'))) {

            ans += decodeWays(code, index + 2, memo);

        }

        return memo[index] = ans;
    }

}
