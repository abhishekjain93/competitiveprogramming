public class RemoveDuplicatesFromList {


    public ListNode removeDuplicates(ListNode A) {

        ListNode cur = A;

        while (cur.next != null) {

            if (cur.val == cur.next.val)
                cur.next = cur.next.next;

            else
                cur = cur.next;

        }

        return A;
    }

    public static void main(String[] args) {

        ListNode n1 = new ListNode(2);
        ListNode n2 = new ListNode(2);
        ListNode n3 = new ListNode(2);
        ListNode n4 = new ListNode(2);
        ListNode n5 = new ListNode(2);

        //n1.next = n2;
       // n2.next = n3;
        //n3.next = n4;
        //n4.next = n5;

        System.out.println(n1);
        System.out.println(new RemoveDuplicatesFromList().removeDuplicates(n1));


    }

}
