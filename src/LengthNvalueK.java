import java.util.Arrays;
import java.util.List;

public class LengthNvalueK {

    public int solve(List<Integer> A, int B, int C) {

        int res = 0;
        int listSize = A.size();
        String CStr = "" + C;
        int CLen = CStr.length();

        if (B > CLen || B <= 0 || listSize == 0) return 0;

        boolean hasZero = A.get(0) == 0 ? true : false;

        if (B < CLen) {

            if (B == 1)
                return listSize;
            if (hasZero)
                return (int) ((listSize - 1) * Math.pow(listSize, B - 1));
            else
                return (int) (Math.pow(listSize, B));

        }

        //case: B == CLen
        int[] count = new int[10];

        for (int i : A) {
            count[i] = 1;

        }
        for (int i = 1; i < 10; i++) {
            count[i] += count[i - 1];
        }

        if (B == 1) {
            return count[CStr.charAt(0) - '0' - 1];
        }

        boolean flag = true;
        for (int i = 1; i <= B; i++) {

            if (!flag) break;

            int digit = CStr.charAt(i - 1) - '0';

            if (digit == 0) {

                flag = count[0] == 0 ? false : true;
                continue;
            }
            res += (count[digit - 1] * Math.pow(listSize, B - i));
            flag = count[digit - 1] == count[digit] ? false : true;

        }
        if (hasZero)
            res -= Math.pow(listSize, B - 1);

        return res;
    }

    public static void main(String[] args) {

        System.out.println(new LengthNvalueK().solve(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), 5, 10004));
    }
}
