import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class Histogram {
	public int largestRectangleArea(ArrayList<Integer> a) {

		Stack<Integer> stack = new Stack<>();
		int size = a.size();
		int res = Integer.MIN_VALUE;

		for (int i = 0; i < size; i++) {

			if (stack.empty() || a.get(stack.peek()) <= a.get(i))
				stack.push(i);

			else {
				while (!stack.empty() && a.get(stack.peek()) > a.get(i)) {

					int index = stack.pop();
					res = Math.max(res, a.get(index) * (stack.empty() ? i : i - 1 - stack.peek()));

				}
				stack.push(i);
			}
		}

		while (!stack.empty()) {
			int index = stack.pop();
			res = Math.max(res, a.get(index) * (stack.empty() ? size : size - 1 - stack.peek()));
		}

		return res;

	}

	public static void main(String[] args) {

		System.out.println(new Histogram().largestRectangleArea(new ArrayList<>(Arrays.asList(90, 58, 69, 70, 82, 100, 13, 57, 47, 18))));
	}
}