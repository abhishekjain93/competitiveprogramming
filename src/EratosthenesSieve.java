import java.util.Arrays;

public class EratosthenesSieve {

	public boolean[] wrongprimes(int a) {

		boolean prime[] = new boolean[a + 1];

		prime[2] = true;
		for (int i = 3; i <= a; i += 2)
			prime[i] = true;

		int count = 0;
		for (int i = 3; i * i <= a; i++) {
			for (int j = i * i; j <= a; j += 2 * i) {
				count++;
				if (prime[j] == false)
					continue;

				prime[j] = false;

			}
		}
		System.out.println("wrong:" + count);
		return prime;
	}

	public boolean[] correctprimes(int a) {

		boolean prime[] = new boolean[a + 1];

		prime[2] = true;
		for (int i = 3; i <= a; i += 2)
			prime[i] = true;
		int count = 0;
		for (int i = 3; i * i <= a; i++) {
			if (prime[i] == false)
				continue;
			for (int j = i * i; j <= a; j += 2 * i) {
				count++;
				prime[j] = false;

			}
		}
		System.out.println("right:" + count);
		return prime;
	}

	public static void main(String[] args) {

		System.out.println(Arrays.equals(new EratosthenesSieve().wrongprimes(10000),
				new EratosthenesSieve().correctprimes(10000)));
		System.out.println(Arrays.toString(new EratosthenesSieve().wrongprimes(100)));

	}

}
