public class PalindromeList {

    public int lPalin(ListNode A) {

        ListNode start = A, p = A, q = A;


        while (q.next != null && q.next.next != null) {

            p = p.next;
            q = q.next.next;

        }

        ListNode x = p, y = p.next, z = null;
        if (p.next != null)
            z = p.next.next;

        while (y != null) {

            y.next = x;
            x = y;
            y = z;

            if (y != null)
                z = y.next;

        }

        while (x != p) {

            if (start.val != x.val)
                return 0;

            start = start.next;
            x = x.next;

        }

        return 1;
    }
}

class ListNode {
    public int val;
    public ListNode next;

    ListNode(int x) {
        val = x;
        next = null;
    }

    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append(val).append("->");

        ListNode n = next;
        while (n != null) {

            sb.append(n.val).append("->");
            n = n.next;
        }

        return sb.toString();
    }


}

