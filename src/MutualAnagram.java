import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MutualAnagram {
    public ArrayList<ArrayList<Integer>> anagrams(final List<String> a) {

        Map<String, Integer> map = new HashMap<>();
        int size = a.size();
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        for (int i = 0; i < size; i++) {

            String s = a.get(i);
            char[] ca = s.toCharArray();
            Arrays.sort(ca);
            String k = String.valueOf(ca);

            if (map.containsKey(k)) {

                res.get(map.get(k))
                   .add(i + 1);
            } else {

                ArrayList<Integer> row = new ArrayList<>();
                res.add(row);
                map.put(k, res.size() - 1);
                row.add(i + 1);
            }
        }
        return res;

    }
}