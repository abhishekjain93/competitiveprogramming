import java.util.List;

public class StocksBuy {

    public int maxProfit(final List<Integer> priceOnDay) {

        if (priceOnDay.size() < 2) return 0;

        int[] maxAhead = new int[priceOnDay.size()];

        maxAhead[priceOnDay.size() - 1] = priceOnDay.get(priceOnDay.size() - 1);

        for (int i = priceOnDay.size() - 2; i >= 0; i--) {

            maxAhead[i] = Math.max(maxAhead[i + 1], priceOnDay.get(i));
        }

        int maxProfit = 0;

        for (int i = 0; i < priceOnDay.size(); i++) {

            maxProfit = Math.max(maxProfit, maxAhead[i] - priceOnDay.get(i));
        }

        return maxProfit;
    }

}
