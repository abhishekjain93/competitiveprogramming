public class Stairs {

    public static void main(String[] args) {

        System.out.println(new Stairs().climbStairs(4));

    }

    public int climbStairs(int n) {

        if (n == 0) return 0;
        if (n == 1) return 1;
        if (n == 2) return 2;

        int prev_2 = 1, prev_1 = 2, cur = 0;

        for (int i = 3; i <= n; i++) {

            cur = prev_1 + prev_2;
            prev_2 = prev_1;
            prev_1 = cur;

        }
        return cur;
    }
}
