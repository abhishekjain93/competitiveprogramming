public class SeasonAmplitude {

    public String solution(int[] T) {

        int seasonDuration = T.length / 4;

        //for winter
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int amp = 0;
        String answer = "WINTER";

        for (int i = 0; i < seasonDuration; i++) {

            min = Math.min(T[i], min);
            max = Math.max(T[i], max);
        }
        amp = max - min;

        //for spring
        min = Integer.MAX_VALUE;
        max = Integer.MIN_VALUE;

        for (int i = seasonDuration; i < 2 * seasonDuration; i++) {

            min = Math.min(T[i], min);
            max = Math.max(T[i], max);
        }

        if (max - min > amp) {

            amp = max - min;
            answer = "SPRING";
        }

        //for summer
        min = Integer.MAX_VALUE;
        max = Integer.MIN_VALUE;

        for (int i = 2 * seasonDuration; i < 3 * seasonDuration; i++) {

            min = Math.min(T[i], min);
            max = Math.max(T[i], max);
        }
        if (max - min > amp) {

            amp = max - min;
            answer = "SUMMER";
        }

        //for autumn
        min = Integer.MAX_VALUE;
        max = Integer.MIN_VALUE;

        for (int i = 3 * seasonDuration; i < 4 * seasonDuration; i++) {

            min = Math.min(T[i], min);
            max = Math.max(T[i], max);
        }
        if (max - min > amp) {

            amp = max - min;
            answer = "AUTUMN";
        }

        return answer;

    }
}
