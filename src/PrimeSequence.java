import java.util.ArrayList;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

public class PrimeSequence {

	public ArrayList<Integer> solve(int A, int B, int C, int D) {

		Set<Integer> visited = new HashSet<>();
		ArrayList<Integer> res = new ArrayList<>();
		PriorityQueue<Integer> minheap = new PriorityQueue<>();
		int count = D;
		minheap.add(A);
		minheap.add(B);
		minheap.add(C);

		while (count != 0) {

			int x = minheap.poll();
			if (visited.contains(x))
				continue;

			visited.add(x);
			count--;
			res.add(x);
			minheap.add(x * A);
			minheap.add(x * B);
			minheap.add(x * C);

		}

		return res;

	}

	public static void main(String[] args) {
		System.out.println(new PrimeSequence().solve(3, 11, 7, 50));
	}
}
