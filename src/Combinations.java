import java.util.ArrayList;


public class Combinations {

    public static void main(String[] args) {

        System.out.println(new Combinations().combine(3, 2));
    }


    public ArrayList<ArrayList<Integer>> combine(int n, int k) {

        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        combineHelper(n, k, 1, new ArrayList<>(), res);

        return res;
    }

    private void combineHelper(int n, int k, int start, ArrayList<Integer> current, ArrayList<ArrayList<Integer>> res) {

        if (k == 0) {

            res.add(new ArrayList<>(current));
            return;
        }
        if (start > n) return;
        current.add(start);
        combineHelper(n, k - 1, start + 1, current, res);
        current.remove(current.size() - 1);
        combineHelper(n, k, start + 1, current, res);

    }
}
