public class RemoveDuplicatesFromListII {

    public ListNode deleteDuplicates(ListNode A) {

        ListNode cur = A;
        ListNode head = new ListNode(0);
        ListNode hp = head;

        while (cur != null) {

            ListNode s = cur;
            int val = cur.val;
            int count = 0;
            while (cur != null && cur.val == val) {

                cur = cur.next;
                count++;
            }

            if (count == 1) {

                hp.next = s;
                hp = hp.next;
            }
        }
        hp.next = null;

        return head.next;
    }

    public static void main(String[] args) {

        ListNode n1 = new ListNode(3);
        ListNode n2 = new ListNode(3);
        ListNode n3 = new ListNode(5);
        ListNode n4 = new ListNode(5);
        ListNode n5 = new ListNode(5);

        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;

        System.out.println(n1);
        System.out.println(new RemoveDuplicatesFromListII().deleteDuplicates(n1));


    }
}
