import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class SortHotelsList {

    public static void main(String[] args) {
        Map<String, Integer> list = new HashMap<String, Integer>();
        Scanner in = new Scanner(System.in);//.toLowerCase()
        String key_words = in.nextLine();
        String[] key_word = key_words.split("\\s+");
        int M = in.nextInt();
        in.nextLine(); //which is the number of reviews
        String[] reviews = new String[2 * M];
        int count;
        for (int i = 0; i < M; i++) {
            count = 0;
            ///System.out.println(i);
            //alternates an hotel ID and a review belonging to that hotel.
            String s1 = in.nextLine(); //ID
            String s2 = in.nextLine();//review

            for (String s : key_word) {
                if (s2.contains(s))
                    count++;
            }
            if (list.get(s1) != null)
                list.put(s1, count + list.get(s1));
            else
                list.put(s1, count);
        }
        //Input
        //breakfast beach citycenter location metro view staff price
        //Output
        //A list of hotel IDs sorted, in descending order, by how many mentions they have of the
        //words specified in the input.

        System.out.println("TOTAL LIST\n" + list.keySet()
                                                .toString() + ":" + list.values()
                                                                        .toString());

        in.close();
    }

    /*  static List<Integer> sort_hotels(String keywords, List<Integer> hotel_ids, List<String> reviews) {

          List<Integer> sortedHotels = new ArrayList<>();

          List<String> dWords = Arrays.asList(keywords.split("\\s+"));

          Map<Integer, List<String>> hotelReviews = new HashMap<>();

          for (int i = 0; i < hotel_ids.size(); i++) {

              if (hotelReviews.containsKey(hotel_ids.get(i))) {

                  hotelReviews.get(hotel_ids.get(i))
                              .add(reviews.get(i));

              } else {

                  hotelReviews.put(hotel_ids.get(i), new ArrayList<>());
                  hotelReviews.get(hotel_ids.get(i))
                              .add(reviews.get(i));

              }

          }

          Map<Integer, Integer> hotelReviewCount = new HashMap<>();

          for (Map.Entry<Integer, List<String>> hotelEntry : hotelReviews.entrySet()) {

              int count = 0;
              List<String> hReviews = hotelEntry.getValue();

              for (int i = 0; i < hReviews.size(); i++) {

                  for (String dW : dWords) {

                      count += Arrays.stream(hReviews.get(i)
                                                     .split("[ ,\\.]"))
                                     .filter(s -> s.equalsIgnoreCase(dW))
                                     .count();

                  }

              }

              hotelReviewCount.put(hotelEntry.getKey(), count);
          }

          hotelReviewCount = hotelReviewCount
                  .entrySet()
                  .stream()
                  .sorted(Map.Entry.comparingByKey())
                  .collect(
                          Collectors.toMap(integerIntegerEntry1 -> integerIntegerEntry1.getKey(),
                          integerIntegerEntry2 ->
                                                   integerIntegerEntry2.getValue(),
                                           (e1, e2) -> e2, LinkedHashMap::new));

          hotelReviewCount = hotelReviewCount
                  .entrySet()
                  .stream()
                  .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                  .collect(
                          Collectors.toMap(integerIntegerEntry -> integerIntegerEntry.getKey(), integerIntegerEntry1 ->
                                                   integerIntegerEntry1.getValue(),
                                           (e1, e2) -> e2, LinkedHashMap::new));

          return new ArrayList<>(hotelReviewCount.keySet());
      }
  */
    static int triangle(int a, int b, int c) {

        if (a < b + c && b < a + c && c < a + b) {

            if (a == b && b == c) {
                return 1;
            }
            return 2;

        }

        return 0;
    }

    static int[] delta_encode(int[] sequence) {

        if (sequence == null || sequence.length == 0) {
            return sequence;
        }

        int size = sequence.length;
        ArrayList<Integer> result = new ArrayList<>();
        result.add(sequence[0]);

        for (int i = 1; i < size; i++) {

            int diff = sequence[i] - sequence[i - 1];
            if (Math.abs(diff) > 127) {

                result.add(-128);
            }
            result.add(diff);

        }

        return result.stream()
                     .mapToInt(SortHotelsList::applyAsInt)
                     .toArray();
    }

    static List<Integer> sort_hotels(String keywords, List<Integer> hotelIds, List<String> reviews) {

        Map<Integer, Integer> list = new HashMap<>();
        String[] keys = keywords.toLowerCase()
                                .split("\\s+");

        HashSet<String> words = new HashSet<>(Arrays.asList(keys));

        int size = reviews.size();
        int c;

        for (int i = 0; i < size; i++) {
            c = 0;

            String[] review = reviews.get(i)
                                     .toLowerCase()
                                     .split("[\\s,;]+");
            for (String s1 : review) {
                if (words.contains(s1))
                    c++;
            }

            if (list.get(hotelIds.get(i)) != null)
                list.put(hotelIds.get(i), c + list.get(hotelIds.get(i)));
            else
                list.put(hotelIds.get(i), c);
        }

        List<Map.Entry<Integer, Integer>> list1 = new LinkedList<>(list.entrySet());

        list1.sort((o1, o2) -> (o2.getValue()).compareTo(o1.getValue()));

        ArrayList<Integer> res = new ArrayList<>();
        for (Map.Entry<Integer, Integer> aa : list1) {
            res.add(aa.getKey());
        }

        return res;

    }

    private static int applyAsInt(Integer i) {
        return i;
    }

    public int numberOfAgents(int initialAgentCount, List<List<Integer>> callsTimes) {

        TreeMap<Integer, Integer> sortedTimes = new TreeMap<>();

        for (int i = 0; i < callsTimes.size(); i++) {

            int arrival = callsTimes.get(i)
                                    .get(0);

            int departure = callsTimes.get(i)
                                      .get(1);

            if (sortedTimes.containsKey(arrival)) {
                sortedTimes.put(arrival, sortedTimes.get(arrival) + 1);

            } else {

                sortedTimes.put(arrival, 1);

            }
            if (sortedTimes.containsKey(departure)) {
                sortedTimes.put(departure, sortedTimes.get(departure) - 1);

            } else {

                sortedTimes.put(departure, -1);

            }

        }
        int current = 0;
        int max = 0;

        for (int x : sortedTimes.values()) {

            current += x;
            max = Math.max(max, current);

        }

        return max > initialAgentCount ? max - initialAgentCount : 0;
    }

}
