import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HotelReviews {

    public static void main(String[] args) {

        System.out.println(new HotelReviews().sort(
                "cool ice wifi", Arrays.asList(1, 2, 3),
                new ArrayList<>(Arrays.asList("water is cool", "cold ice drink", "cool wifi speed"))));
    }

    public List<Integer> sort(String dict, List<Integer> hotelIds, List<String> reviews) {

        int size = reviews.size();
        Map<Integer, Integer> goodCount = new HashMap<>();
        String[] dwords = dict.split("\\s+");

        Trie trie = new Trie();
        for (String w : dwords) {
            trie.insert(w.toLowerCase());

        }
        for (int i = 0; i < size; i++) {
            String[] rwords = reviews.get(i)
                                     .split("[\\s,;]+");
            for (String r : rwords) {

                if (trie.search(r.toLowerCase())) {

                    if (!goodCount.containsKey(hotelIds.get(i))) {

                        goodCount.put(hotelIds.get(i), 1);

                    } else {

                        goodCount.put(hotelIds.get(i), goodCount.get(hotelIds.get(i)) + 1);
                    }

                }

            }
        }
        hotelIds.sort(new ReviewComparator(goodCount));
        return hotelIds;
    }

    public static class ReviewComparator implements Comparator<Integer> {

        Map<Integer, Integer> goodcount;

        ReviewComparator(Map<Integer, Integer> goodcount) {
            super();
            this.goodcount = goodcount;
        }

        @Override
        public int compare(Integer o1, Integer o2) {

            return Integer.compare(goodcount.get(o2), goodcount.get(o1));
        }

    }

    public static class TrieNode {

        TrieNode[] links = new TrieNode[26];
        boolean isword;
        String word;

    }

    public static class Trie {

        TrieNode node = new TrieNode();

        void insert(String word) {

            TrieNode temp = node;
            int len = word.length();
            for (int i = 0; i < len; i++) {

                if (temp.links[word.charAt(i) - 'a'] == null)
                    temp.links[word.charAt(i) - 'a'] = new TrieNode();

                temp = temp.links[word.charAt(i) - 'a'];

            }
            temp.isword = true;
            temp.word = word;

        }

        boolean search(String word) {

            TrieNode temp = node;
            int len = word.length();
            for (int i = 0; i < len; i++) {

                if (temp.links[word.charAt(i) - 'a'] == null)
                    return false;

                temp = temp.links[word.charAt(i) - 'a'];

            }

            return temp.isword;
        }
    }
}
