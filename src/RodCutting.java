import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class RodCutting {

    public static void main(String[] args) {

        System.out.println(new RodCutting().rodCut(6, new ArrayList<>(Arrays.asList(1, 2, 5))));

    }

    public ArrayList<Integer> rodCut(int N, ArrayList<Integer> cuts) {

        ArrayList<Integer> result = new ArrayList<>();

        Map<Pair, Integer> parent = new HashMap<>();
        Map<Pair, Long> dp = new HashMap<>();

        minCutHelper(0, N, cuts, parent, dp);

        resultFromParentPointers(parent, result, 0, N);
        return result;

    }

    private void resultFromParentPointers(Map<Pair, Integer> parent, List<Integer> result, int start, int end) {

        final Pair pair = new Pair(start, end);
        if (!parent.containsKey(pair)) return;

        final int cut = parent.get(pair);

        if (cut != -1) {

            result.add(cut);
            resultFromParentPointers(parent, result, start, cut);
            resultFromParentPointers(parent, result, cut, end);

        }

    }

    private long minCutHelper(int start, int end, List<Integer> cuts, final Map<Pair, Integer> parent, final Map<Pair,
            Long> dp) {

        final Pair pair = new Pair(start, end);

        if (dp.containsKey(pair)) return dp.get(pair);
        if (cuts.isEmpty()) {

            parent.put(pair, -1);
            dp.put(pair, 0l);
            return 0;
        }
        long min = Long.MAX_VALUE;
        int len = end - start;
        int cutIdx = -1;
        for (int i = 0; i < cuts.size(); i++) {

            long current = len + minCutHelper(start, cuts.get(i), cuts.subList(0, i), parent, dp)
                    + minCutHelper(cuts.get(i), end, cuts.subList(i + 1, cuts.size()), parent, dp);

            if (current < min) {

                min = current;
                cutIdx = cuts.get(i);

            }
        }

        if (cutIdx != -1) {
            parent.put(pair, cutIdx);
        }

        dp.put(pair, min);
        return min;
    }

    class Pair {
        int x = 0;
        int y = 0;

        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair pair = (Pair) o;
            return x == pair.x &&
                    y == pair.y;
        }

        @Override
        public int hashCode() {

            return Objects.hash(x, y);
        }

        @Override
        public String toString() {
            return "{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

}
