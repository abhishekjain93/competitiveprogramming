import java.util.ArrayList;
import java.util.Arrays;

class Interval {
	int start;
	int end;

	Interval() {
		start = 0;
		end = 0;
	}

	Interval(int s, int e) {
		start = s;
		end = e;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[" + start + " ," + end + "]";
	}
}

public class AddInterval {

	public ArrayList<Interval> merge(ArrayList<Interval> list, Interval interval) {

		ArrayList<Interval> res = new ArrayList<>();
		Interval merged = new Interval();
		int size = list.size();
		int i = 0;
		int min = 0, max = 0;

		while (i < size && list.get(i).end < interval.start) {
			res.add(list.get(i));
			i++;
		}

		if (i == size) {

			res.add(interval);
			return res;

		}
		min = Math.min(list.get(i).start, interval.start);

		while (i < size && list.get(i).end < interval.end) {

			i++;
		}

		if (i == size) {

			max = interval.end;
			merged.start = min;
			merged.end = max;
			res.add(merged);
			return res;
		}

		if (list.get(i).start <= interval.end) {
			max = list.get(i).end;
			i++;
		} else {

			max = interval.end;
		}
		merged.start = min;
		merged.end = max;
		res.add(merged);

		while (i < size) {

			res.add(list.get(i++));
		}

		return res;

	}

	public static void main(String[] args) {

		Interval interval = new Interval(4, 9);

		Interval i1 = new Interval(1, 2);
		Interval i2 = new Interval(3, 4);

		Interval i3 = new Interval(6, 7);
		Interval i4 = new Interval(8, 10);
		Interval i5 = new Interval(12, 16);

		ArrayList<Interval> list = new ArrayList<>(Arrays.asList(i1, i2));
		System.out.println(new AddInterval().merge(list, interval));

	}

}
