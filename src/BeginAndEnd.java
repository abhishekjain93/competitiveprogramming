import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BeginAndEnd {

	public ArrayList<Integer> searchRange(final List<Integer> a, int b) {

		int size = a.size();
		int start = 0;
		int end = size - 1;
		int mid = 0;
		int first = 0;
		int last = 0;

		while (start <= end) {
			mid = (start + end) / 2;

			System.out.println(mid);
			if (a.get(mid) < b) {

				start = mid+1;
				continue;
			} else if (a.get(mid) > b) {

				end = mid-1;
				continue;
			} else {

				if (mid == 0 || a.get(mid - 1) < b) {

					first = mid;
					break;
				}
				end = mid-1;

			}

		}
		if (start > end) {

			ArrayList<Integer> result = new ArrayList<>();
			result.add(-1);
			result.add(-1);
			return result;

		}

		start = 0;
		end = size - 1;
		mid = 0;
		last = 0;

		while (start <= end) {
			mid = (start + end) / 2;
			System.out.println(mid);
			if (a.get(mid) < b) {

				start = mid+1;
				continue;
			} else if (a.get(mid) > b) {

				end = mid-1;
				continue;
			} else {

				if (mid == size - 1 || a.get(mid + 1) > b) {

					last = mid;
					break;
				}
				start = mid+1;

			}

		}

		ArrayList<Integer> result = new ArrayList<>();
		result.add(first);
		result.add(last);
		return result;

	}

	public static void main(String[] args) {

		System.out.println(new BeginAndEnd().searchRange(new ArrayList<>(Arrays.asList(1)), 10));

	}

}
