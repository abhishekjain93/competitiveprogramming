import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class MergeKLists {

    public ListNode mergeKLists(ArrayList<ListNode> list) {

        int size = list.size();
        ListNode res = new ListNode(0), p = res;

        PriorityQueue<ListNode> heap = new PriorityQueue<>(new Comparator<ListNode>() {
            @Override
            public int compare(ListNode o1, ListNode o2) {
                return Integer.compare(o1.val, o2.val);
            }
        });

        for (int i = 0; i < size; i++) {

            heap.add(list.get(i));
        }

        while (!heap.isEmpty()) {

            ListNode poll = heap.poll();
            p.next = poll;
            p = p.next;
            if (poll.next != null)
                heap.add(poll.next);

        }
        return res.next;
    }
}
