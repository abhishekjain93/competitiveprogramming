import java.util.*;

public class LetterPhone {

    public static void main(String[] args) {

        System.out.println(new LetterPhone().letterCombinations("2"));

    }

    Map<Character, List<String>> lookup = new HashMap<>();

    {
        lookup.put('0', Arrays.asList("0"));
        lookup.put('1', Arrays.asList("1"));
        lookup.put('2', Arrays.asList("a", "b", "c"));
        lookup.put('3', Arrays.asList("d", "e", "f"));
        lookup.put('4', Arrays.asList("g", "h", "i"));
        lookup.put('5', Arrays.asList("j", "k", "l"));
        lookup.put('6', Arrays.asList("m", "n", "o"));
        lookup.put('7', Arrays.asList("p", "q", "r", "s"));
        lookup.put('8', Arrays.asList("t", "u", "v"));
        lookup.put('9', Arrays.asList("w", "x", "y", "z"));

    }

    public String[] letterCombinations(String digits) {

        if (digits == null) return new String[]{};
        if (digits.length() == 1) {

            return lookup.get(digits.charAt(0)).toArray(new String[1]);

        }

        String[] rem = letterCombinations(digits.substring(1));
        List<String> addChars = lookup.get(digits.charAt(0));
        List<String> result = new ArrayList<>();

        for (String x : addChars) {

            for (String str : rem) {

                result.add(x + str);
            }
        }

        return result.toArray(new String[result.size()]);
    }

}
