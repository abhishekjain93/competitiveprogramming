import java.util.Arrays;

public class DistributeCandy {

    public static void main(String[] args) {
        System.out.println(new DistributeCandy().candy(new int[]{5, 5, 4, 3, 2, 1}));
    }

    public int candy(int[] rating) {

        int length = rating.length;
        int[] candies = new int[length];

        for (int i = 0; i < length; i++) {

            candies[i] = 1;

        }

        for (int i = 1; i < length; i++) {

            if (rating[i] > rating[i - 1]) {
                candies[i] = candies[i - 1] + 1;
            }

        }

        for (int i = length - 2; i >= 0; i--) {

            if (rating[i] > rating[i + 1]) {

                candies[i] = Math.max(candies[i], candies[i + 1] + 1);
            }

        }

        return Arrays.stream(candies)
                     .sum();
    }

}
