import java.util.ArrayList;
import java.util.Arrays;

public class Merge2Arrays {

	public void merge(ArrayList<Integer> a, ArrayList<Integer> b) {

		int alength = a.size();
		int blength = b.size();
		for (int i = 0; i < blength; i++) {
			a.add(0);

		}
		int i = alength - 1;
		int j = blength - 1;
		int k = alength + blength - 1;

		while (k > -1) {

			if (i < 0 || j < 0)
				break;
			if (a.get(i) >= b.get(j)) {

				a.set(k, a.get(i));
				i--;
			} else {

				a.set(k, b.get(j));
				j--;
			}
			k--;

		}
		if (i < 0)
			while (k > -1)
				a.set(k--, b.get(j--));

		if (j < 0)
			while (k > -1)
				a.set(k--, a.get(i--));

	}

	public static void main(String[] args) {

		ArrayList<Integer> a = new ArrayList<>(Arrays.asList(-4, 3));
		ArrayList<Integer> b = new ArrayList<>(Arrays.asList(-2, -2));

		new Merge2Arrays().merge(a, b);
		System.out.println(a);

	}
}
