public class Add2Lists {

    public static void main(String[] args) {
        ListNode n1 = new ListNode(2);
        ListNode n2 = new ListNode(4);
        ListNode n3 = new ListNode(1);
        ListNode n4 = new ListNode(3);
        ListNode n5 = new ListNode(6);

        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;
        System.out.println(n1);

        ListNode m1 = new ListNode(5);
        ListNode m2 = new ListNode(6);
        ListNode m3 = new ListNode(4);
        ListNode m4 = new ListNode(9);
        ListNode m5 = new ListNode(6);

        m1.next = m2;
        m2.next = m3;
        m3.next = m4;
        //m4.next = m5;
        System.out.println(m1);
        System.out.println(new Add2Lists().addTwoNumbers(n1, m1));


    }

    public ListNode addTwoNumbers(ListNode A, ListNode B) {


        ListNode res = new ListNode(0);
        ListNode a = A, b = B, x = res;

        int carry = 0, sum = 0;
        while (a != null && b != null) {

            sum = a.val + b.val + carry;
            carry = sum / 10;
            sum = sum % 10;
            x.next = new ListNode(sum);
            a = a.next;
            b = b.next;
            x = x.next;

        }
        while (a != null) {
            sum = a.val + carry;
            carry = sum / 10;
            sum = sum % 10;
            x.next = new ListNode(sum);
            x = x.next;
            a = a.next;
        }
        while (b != null) {
            sum = b.val + carry;
            carry = sum / 10;
            sum = sum % 10;
            x.next = new ListNode(sum);
            x = x.next;
            b = b.next;
        }

        if (carry == 1)
            x.next = new ListNode(1);

        return res.next;
    }
}
