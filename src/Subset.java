import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class Subset {

	ArrayList<ArrayList<Integer>> subsets(ArrayList<Integer> a) {

	    Collections.sort(a);
		ArrayList<ArrayList<Integer>> res = new ArrayList<>();
		Set<ArrayList<Integer>> set = new HashSet<>();

		int size = a.size();
		boolean flag[] = new boolean[size];
		subsethelper(set, a, flag, size - 1, size);
		res.addAll(set);
		Collections.sort(res, new ListComparator());
		return res;
	}

	public void subsethelper(Set<ArrayList<Integer>> res, ArrayList<Integer> a, boolean[] flag, int i, int n) {

		if (i == -1) {

			ArrayList<Integer> tuple = new ArrayList<Integer>();

			for (int l = 0; l < n; l++) {

				if (flag[l])
					tuple.add(a.get(l));

			}
			res.add(tuple);
			return;

		}

		flag[i] = false;
		subsethelper(res, a, flag, i - 1, n);
		flag[i] = true;
		subsethelper(res, a, flag, i - 1, n);

	}

	public static void main(String[] args) {
		System.out.println(new Subset().subsets(new ArrayList<Integer>(Arrays.asList(15, 20, 12, 19, 4))));
	}

	public static class ListComparator implements Comparator<ArrayList<Integer>> {

		@Override
		public int compare(ArrayList<Integer> o1, ArrayList<Integer> o2) {

			int minlen = o1.size() > o2.size() ? o2.size() : o1.size();
			for (int i = 0; i < minlen; i++) {

				if (o1.get(i) != o2.get(i))
					return o1.get(i).compareTo(o2.get(i));

			}
			return Integer.compare(o1.size(), o2.size());
		}

	}
}
