import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SmsSplitter {

    static ArrayList<String> splitText(String message, int charLimit) {

        return splitTextAuxUsingSplit(message, charLimit);
    }

    public static List<Integer> connectedCities(int n, int g, List<Integer> originCities, List<Integer>
            destinationCities) {
        // Write your code here

        ArrayList<Integer> graph[] = new ArrayList[n];
        for (int i = 0; i < n; i++) {
            graph[i] = new ArrayList<>();
        }

        int len = originCities.size();
        for (int i = 0; i < len; i++) {
            if (gcd(originCities.get(i), destinationCities.get(i)) > g) {
                graph[originCities.get(i)].add(destinationCities.get(i));
                graph[destinationCities.get(i)].add(originCities.get(i));
            }

        }

        return null;

    }

    public static Integer gcd(Integer p, Integer q) {
        if (q == 0) {
            return p;
        }
        return gcd(q, p % q);
    }

    static ArrayList<String> splitTextAuxUsingSplit(String message, int charLimitOriginal) {
        //Decrease the char limit to accomodate chunk number at the end i.e. (1/3). For now assuming, the message
        // chunks won't be more than 9
        int charLimit = charLimitOriginal - 5;
        //New arraylist to store message chunks
        ArrayList<String> result = new ArrayList<String>();
        String[] splitted = message.split(" ");
        String temp;

        for (int i = 0; i < splitted.length - 1; i++) {
            temp = splitted[i];
            //while length of temp and the next element combined is less than charLimit, temp = temp + next element
            //Last element to be taken care of after this loop
            while ((temp + 1 + splitted[i + 1]).length() <= charLimit && i + 1 < splitted.length - 1) {  //+1 for space
                temp = temp + " " + splitted[i + 1];
                i++;
            }
            result.add(temp);

        }
        //Take care of the last element
        //Add the last element from splitted to the last element of result if their combined length is less than
        // charLimit
        String lastElement = result.get(result.size() - 1);
        if (lastElement.length() + 1 + splitted[splitted.length - 1].length() < charLimit) {  //+1 for space

            result.set(result.size() - 1, lastElement + " " + splitted[splitted.length - 1]);
        } else {
            result.add(splitted[splitted.length - 1]);
        }

        //append message chunk number for ex (1/3)
        int resultSize = result.size();
        for (int i = 0; i < resultSize; i++) {
            result.set(i, result.get(i) + "(" + (i + 1) + "/" + resultSize + ")");
        }

        return result;
    }

    public static void main(String[] args) {
        String message;
        int charLmit;
        message = "Hi Sivasrinivas, your Uber is arriving now! And this is a bigger text";

        message = "The best lies are always mixed with a little truth";
        charLmit = 30;
        ArrayList<String> result = splitText(message, charLmit);
        for (String item : result) {
            System.out.println("Length = " + item.length() + " : " + item);
        }
        System.out.println(result.toString());
    }

    Boolean isReachable(int s, int d, ArrayList<Integer> adj[]) {
        LinkedList<Integer> temp;

        // Mark all the vertices as not visited(By default set
        // as false)
        boolean visited[] = new boolean[adj.length];

        // Create a queue for BFS
        LinkedList<Integer> queue = new LinkedList<Integer>();

        // Mark the current node as visited and enqueue it
        visited[s] = true;
        queue.add(s);

        // 'i' will be used to get all adjacent vertices of a vertex
        Iterator<Integer> i;
        while (queue.size() != 0) {
            // Dequeue a vertex from queue and print it
            s = queue.poll();

            int n;
            i = adj[s].listIterator();

            // Get all adjacent vertices of the dequeued vertex s
            // If a adjacent has not been visited, then mark it
            // visited and enqueue it
            while (i.hasNext()) {
                n = i.next();

                // If this adjacent node is the destination node,
                // then return true
                if (n == d)
                    return true;

                // Else, continue to do BFS
                if (!visited[n]) {
                    visited[n] = true;
                    queue.add(n);
                }
            }
        }

        // If BFS is complete without visited d
        return false;
    }
}