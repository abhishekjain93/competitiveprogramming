import java.util.HashMap;

public class Divide {

	public String divide(int a, int b) {

		StringBuilder res = new StringBuilder();
		int q = a / b;
		HashMap<Integer, Integer> map = new HashMap<>();
		int r = a % b;
		res.append(q);

		if (r == 0)
			return res.toString();
		res.append('.');

		while (!map.containsKey(r)) {
			map.put(r, res.length());
			r *= 10;
			q = r / b;
			res.append(q);
			r = r % b;

		}

		res.insert(map.get(r), "(");

		res.append(')');

//		if (res.length() > 3 && res.charAt(res.length() - 1) == ')' && res.charAt(res.length() - 2) == '0')
//			res.replace(res.length() - 3, res.length(), "");
		return res.toString().replace("(0)", "");
	}
	
	public String fractionToDecimal(int numerator, int denominator) {
	    StringBuilder result = new StringBuilder();
	    String sign = (numerator < 0 == denominator < 0 || numerator == 0) ? "" : "-";
	    long num = Math.abs((long) numerator);
	    long den = Math.abs((long) denominator);
	    result.append(sign);
	    result.append(num / den);
	    long remainder = num % den;
	    if (remainder == 0)
	        return result.toString();
	    result.append(".");
	    HashMap<Long, Integer> hashMap = new HashMap<Long, Integer>();
	    while (!hashMap.containsKey(remainder)) {
	        hashMap.put(remainder, result.length());
	        result.append(10 * remainder / den);
	        remainder = 10 * remainder % den;
	    }
	    int index = hashMap.get(remainder);
	    result.insert(index, "(");
	    result.append(")");
	    return result.toString().replace("(0)", "");
	}

	public static void main(String[] args) {

		System.out.println(new Divide().divide(826, 393));
	}

}
