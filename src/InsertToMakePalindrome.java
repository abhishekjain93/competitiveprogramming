import java.util.Arrays;

public class InsertToMakePalindrome {
    public int solve(String A) {

        int length = A.length();

        String txt = A + "^" + new StringBuilder(A).reverse().toString();
        int[] lps = new int[txt.length()];

        getLPS(txt, lps);
        System.out.println(Arrays.toString(lps));
        return length - lps[txt.length() - 1];


    }

    public void getLPS(String x, int[] lps) {

        lps[0] = 0;
        int length = x.length();
        int len = 0;
        for (int i = 1; i < length;) {

            if (x.charAt(len) == x.charAt(i)) {

                lps[i] = len + 1;
                i++;
                len++;

            } else {

                if (len == 0)
                    lps[i++] = 0;
                else
                    len = lps[len - 1];

            }

        }
    }

    public static void main(String[] args) {

        System.out.println(new InsertToMakePalindrome().solve("AAA"));

    }
}