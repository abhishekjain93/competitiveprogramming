import java.util.ArrayList;

public class PalindromePartition {

    public static void main(String[] args) {

        System.out.println(new PalindromePartition().partition("aab"));

    }

    public ArrayList<ArrayList<String>> partition(String s) {

        int len = s.length();
        boolean[][] P = new boolean[len][len];

        for (int i = 0; i < len; i++) {

            P[i][i] = true;
        }

        for (int size = 2; size <= len; size++) {

            for (int start = 0; start < len - size + 1; start++) {

                if ((s.charAt(start) == s.charAt(start + size - 1) && (size == 2 || P[start + 1][start + size - 2])))
                    P[start][start + size - 1] = true;
            }
        }

        ArrayList<ArrayList<String>> res = new ArrayList<ArrayList<String>>();
        partition(s, 0, new ArrayList<String>(), res, P);

        return res;
    }

    public void partition(String s, int start, ArrayList<String> path, ArrayList<ArrayList<String>> res, boolean[][] P) {


        int len = s.length();
        if (start >= len) {
            res.add(new ArrayList<>(path));
        }

        for (int i = start; i < len; i++) {

            if (P[start][i]) {

                path.add(s.substring(start, i + 1));
                partition(s, i + 1, path, res, P);
                path.remove(path.size() - 1);
            }
        }
    }

}
