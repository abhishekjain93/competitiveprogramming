public class ChordsOfACircle {

    public int chordCnt(int n) {

        int[] memo = new int[n + 1];
        memo[0] = 1;
        memo[1] = 1;
        return waysHelper(n, memo);

    }

    public int waysHelper(int n, int[] memo) {

        if (memo[n] != 0) return memo[n];

        long ways = 0;
        for (int i = 0; i < n; i++) {

            ways += ((long)waysHelper(i, memo) * waysHelper(n - i - 1, memo)) % 1000000007;
        }

        return memo[n] = (int) (ways % 1000000007);
    }
}
