import java.math.BigInteger;

public class CompareVersions {

    public int compareVersion(String a, String b) {

        String[] splitA = a.split("\\.");
        String[] splitB = b.split("\\.");

        int lenA = splitA.length;
        int lenB = splitB.length;
        int loopLen = Math.min(lenA, lenB);

        for (int i = 0; i < loopLen; i++) {

            if (new java.math.BigInteger(splitA[i]).equals(new java.math.BigInteger(splitB[i])))
                continue;

            return new java.math.BigInteger(splitA[i]).compareTo(new java.math.BigInteger(splitB[i]));

        }

        if (lenA > loopLen) {

            for (int i = loopLen; i < lenA; i++) {

                if (new java.math.BigInteger(splitA[i]).compareTo(java.math.BigInteger.ZERO) == 1)
                    return 1;
            }

        }
        if (lenB > loopLen) {

            for (int i = loopLen; i < lenB; i++) {

                if (new java.math.BigInteger(splitB[i]).compareTo(java.math.BigInteger.ZERO) == 1)
                    return -1;
            }

        }
        return 0;
    }

    public static void main(String[] args) {

        System.out.println(new CompareVersions().compareVersion("13.0.1", "13"));
    }
}
