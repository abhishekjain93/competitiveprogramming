public class ValidBST {

    public int isValidBST(TreeNode A) {

        if (isValidBSTUtil(A, Integer.MIN_VALUE, Integer.MAX_VALUE)) {
            return 1;
        } else {
            return 0;
        }
    }

    private boolean isValidBSTUtil(TreeNode root, int leftMin, int rightMax) {

        if (root == null) {
            return true;
        }

        if (root.val <= leftMin || root.val >= rightMax) {
            return false;
        }

        return isValidBSTUtil(root.left, leftMin, root.val) && isValidBSTUtil(root.right, root.val, rightMax);

    }

}

