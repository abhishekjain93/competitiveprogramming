import java.util.ArrayList;

public class MinJumpArray {

    public int jump(ArrayList<Integer> list) {

        if (list.size() < 2) return 0;
        if (list.get(0) == 0) return -1;

        int jumps = 1;
        int last = list.size();
        int lastReachable = list.get(0);
        int currentStepsRemaining = list.get(0);

        int i = 1;
        while (i < last) {

            if (i == last - 1)
                return jumps;

            lastReachable = Math.max(lastReachable, i + list.get(i));
            currentStepsRemaining--;

            if (currentStepsRemaining == 0) {

                if (lastReachable <= i) return -1;
                jumps++;
                currentStepsRemaining = lastReachable - i;

            }

            i++;
        }

        return -1;
    }

    public int jump1(ArrayList<Integer> list) {

        int[] memo = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {

            memo[i] = -1;
        }
        memo[0] = 0;
        jumpHelper(list, memo);

        return memo[list.size() - 1];
    }

    public void jumpHelper(ArrayList<Integer> list, int[] memo) {

        for (int i = 1; i < list.size(); i++) {
            int minSteps = Integer.MAX_VALUE;
            for (int j = 0; j < i; j++) {

                if (list.get(j) >= i - j && memo[j] != -1)
                    minSteps = Math.min(minSteps, memo[j] + 1);

            }
            memo[i] = minSteps == Integer.MAX_VALUE ? -1 : minSteps;
        }
    }
}
