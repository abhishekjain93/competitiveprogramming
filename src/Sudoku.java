import java.util.ArrayList;
import java.util.List;

public class Sudoku {

    public static void main(String[] args) {

        int grid[][] = new int[][]
                {
                        {0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0}
                };

        new Sudoku().solve(grid);

        new Sudoku().printBoard(grid);
        System.out.println(new Sudoku().isValidSudokuGrid(grid));
    }

    public int[][] solve(int[][] grid) {

        boolean[] presentNumbers = new boolean[10];
        List<Integer>[][] choices = new ArrayList[9][9];

        for (int i = 0; i < 9; i++) {
            choices[i] = new ArrayList[9];
            for (int j = 0; j < 9; j++) {

                choices[i][j] = new ArrayList<>();

                if (grid[i][j] != 0) {

                    choices[i][j].add(grid[i][j]);

                } else {

                    for (int k = 0; k < 10; k++) {
                        presentNumbers[k] = false;
                    }

                    for (int k = 0; k < 9; k++) {

                        if (grid[i][k] != 0) {
                            presentNumbers[grid[i][k]] = true;
                        }
                    }
                    for (int k = 0; k < 9; k++) {

                        if (grid[k][j] != 0) {
                            presentNumbers[grid[i][k]] = true;
                        }
                    }
                    //for same 3x3
                    for (int k = 0; k < 3; k++) {
                        for (int l = 0; l < 3; l++) {

                            presentNumbers[grid[(i / 3) * 3 + k][(j / 3) * 3 + l]] = true;
                        }
                    }
                    for (int x = 1; x < 10; x++) {

                        if (presentNumbers[x] == false) {
                            choices[i][j].add(x);
                        }
                    }

                    if (choices[i][j].size() == 1) {
                        grid[i][j] = choices[i][j].get(0);
                    }
                }

            }
        }
        // System.out.println(Arrays.deepToString(choices));
        solver(grid, choices);
        return grid;
    }

    private boolean solver(int[][] grid, List<Integer>[][] choices) {

        Pair unassignedPosition = findUnassignedPosition(grid);
        if (unassignedPosition.x == -1) return true;

        int x = unassignedPosition.x;
        int y = unassignedPosition.y;

        for (Integer choice : choices[x][y]) {

            if (isValid(grid, x, y, choice)) {
                grid[x][y] = choice;

                if (solver(grid, choices))
                    return true;

                grid[x][y] = 0;
            }
        }
        return false;

    }

    private boolean isValid(int[][] grid, int row, int column, int choice) {

        for (int k = 0; k < 9; k++) {

            if (grid[row][k] == choice) {
                return false;
            }
        }

        for (int k = 0; k < 9; k++) {

            if (grid[k][column] == choice) {
                return false;
            }
        }

        for (int k = 0; k < 3; k++) {
            for (int l = 0; l < 3; l++) {

                if (grid[(row / 3) * 3 + k][(column / 3) * 3 + l] == choice) return false;

            }
        }

        return true;
    }

    private Pair findUnassignedPosition(int[][] grid) {

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {

                if (grid[i][j] == 0) return new Pair(i, j);

            }
        }
        return new Pair(-1, -1);
    }

    private void printBoard(int[][] grid) {
        for (int row = 0; row < 9; row++) {
            for (int column = 0; column < 9; column++) {
                System.out.print(grid[row][column] + " ");
            }
            System.out.println();
        }
    }

    //We can use 27 different sets for each of row col and box. and check
    /*  I did this once for a class project. I used a total of 27 sets to represent each row, column and box. I'd
    check the numbers as I added them to each set (each placement of a number causes the number to be added to 3
    sets, a row, a column, and a box) to make sure the user only entered the digits 1-9. The only way a set could get
     filled is if it was properly filled with unique digits. If all 27 sets got filled, the puzzle was solved.
     Setting up the mappings from the user interface to the 27 sets was a bit tedious, but made the rest of the logic
      a breeze to implement.
     */  private boolean isValidSudokuGrid(int[][] grid) {

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {

                int candidate = grid[i][j];
                grid[i][j] = 0;
                if (!isValid(grid, i, j, candidate)) return false;
                grid[i][j] = candidate;
            }
        }
        return true;
    }

}

class Pair {
    int x = 0;
    int y = 0;

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

