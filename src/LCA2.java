import java.util.Deque;
import java.util.LinkedList;

public class LCA2 {

    public static void main(String[] args) {

        TreeNode tree = new TreeNode(4);
        tree.left = new TreeNode(8);
        tree.right = new TreeNode(7);
        tree.left.left = new TreeNode(13);
        tree.left.right = new TreeNode(6);

        tree.left.right.left = new TreeNode(9);
        tree.left.right.right = new TreeNode(10);
        tree.left.right.left.left = new TreeNode(12);
        tree.left.right.right.left = new TreeNode(2);
        tree.left.right.right.right = new TreeNode(11);
        tree.left.right.right.right.right = new TreeNode(5);
        tree.left.right.right.right.right.left = new TreeNode(1);
        tree.left.right.right.right.right.left.right = new TreeNode(3);

        System.out.println(new LCA2().lca(tree,7, 3));



    }



    public int lca(TreeNode root, int val1, int val2) {

        Deque<TreeNode> q1 = new LinkedList<>();
        Deque<TreeNode> q2 = new LinkedList<>();

        pathStack(root, val1, q1);
        pathStack(root, val2, q2);

        int lca = -1;
        while (!q1.isEmpty() && !q2.isEmpty() && q1.peek().val == q2.peek().val) {

            lca = q1.peek().val;
            q1.poll();
            q2.poll();

        }

        return lca;
    }

    public void pathStack(TreeNode root, int val, Deque<TreeNode> q) {

        if (root == null) return;

        q.add(root);
        if (root.val == val) return;

        pathStack(root.left, val, q);
        pathStack(root.right, val, q);
        if (q.peekLast() == root) q.removeLast();

    }
}
