public class PartitionList {


    public ListNode partition(ListNode A, int x) {


        ListNode cur = A, lesser = new ListNode(0), remaining = new ListNode(0);

        ListNode p = lesser, q = remaining;

        while (cur != null) {

            if (cur.val < x) {

                p.next = cur;
                p = p.next;

            } else {

                q.next = cur;
                q = q.next;

            }
            cur = cur.next;

        }
        q.next = null;
        p.next = remaining.next;

        return lesser.next;
    }

}
