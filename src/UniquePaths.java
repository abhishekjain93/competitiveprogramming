public class UniquePaths {

    public int uniquePathsWithObstacles(int[][] grid) {

        if (grid[0][0] == 1) return 0;
        int[][] dp = new int[grid.length][grid[0].length];
        dp[0][0] = 1;

        for (int i = 1; i < grid[0].length; i++) {

            if (grid[0][i] == 1)
                dp[0][i] = 0;
            else
                dp[0][i] = dp[0][i - 1];

        }

        for (int i = 1; i < grid.length; i++) {

            if (grid[i][0] == 1)
                dp[i][0] = 0;
            else
                dp[i][0] = dp[i - 1][0];

        }

        for (int i = 1; i < grid.length; i++) {

            for (int j = 1; j < grid[0].length; j++) {

                if (grid[i][j] == 1)
                    dp[i][j] = 0;
                else
                    dp[i][j] = dp[i][j - 1] + dp[i - 1][j];
            }

        }

        return dp[grid.length - 1][grid[0].length - 1];
    }

}
