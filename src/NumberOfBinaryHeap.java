public class NumberOfBinaryHeap {

    public static void main(String[] args) {

        int[] memo = new int[101];
        int[][] memoC = new int[101][101];

        for (int i = 1; i < 100; i++) {

            System.out.println(new NumberOfBinaryHeap().heapCountAssist(i, memo, memoC));
        }
    }


    public int combination(int n, int r, int[][] memo) {

        if (memo[n][r] != 0) return memo[n][r];
        if (n == r || r == 0) return memo[n][r] = 1;

        int ans = (int) (((long)combination(n - 1, r, memo) % 1000000007 + combination(n - 1, r - 1, memo) % 1000000007) % 1000000007);
        return memo[n][r] = ans;

    }

    public int heapCountAssist(int n, int[] memo, int[][] memoC) {

        if (memo[n] != 0) return memo[n];
        if (n <= 1) return memo[n] = 1;

        int height = (int) Math.floor(Math.log(n) / Math.log(2));
        int lastNodes = n - (int) Math.pow(2, height) + 1;

        int nL = (int) (Math.pow(2, height - 1) - 1 + Math.min(Math.pow(2, height - 1), lastNodes));
        int nR = (int) (Math.pow(2, height - 1) - 1 + Math.max(0, lastNodes - Math.pow(2, height - 1)));

        return memo[n] = (int) (combination(n - 1, nL, memoC)* ((long)heapCountAssist(nL, memo, memoC) % 1000000007 * heapCountAssist(nR, memo, memoC) % 1000000007) % 1000000007);

    }

    public int heapCount(int n) {

        int[] memo = new int[101];
        int[][] memoC = new int[101][101];

        return heapCountAssist(n, memo, memoC);

    }
}
