import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RotatedArray {
	// DO NOT MODIFY THE LIST
	public int search(final List<Integer> a, int b) {

		// find the pivot
		int size = a.size();
		int pivot = 0;
		int low = 0, high = a.size() - 1;
		int mid = 0;
		int value = a.get(0);

		while (low <= high) {

			mid = (low + high) / 2;
			if (a.get(mid) < value) {

				high = mid - 1;

			} else if (a.get(mid) > value) {

				if (a.get(mid) > a.get(mid + 1)) {
					pivot = mid;
					break;

				}
				low = mid + 1;
			}
		}
		if (b == value)
			return 0;

		if (b < value)
			return binarySearch(a, pivot + 1, size, b);
		return binarySearch(a, 0, pivot, b);

	}

	public int binarySearch(List<Integer> list, int begin, int end, int query) {
		if (begin > end)
			return -1;
		int middle = (begin + end) / 2;
		if (list.get(middle) == query)
			return middle;
		if (list.get(middle) < query)
			return binarySearch(list, middle + 1, end, query);
		return binarySearch(list, begin, middle -1, query);
	}
	
	public static void main(String[] args) {

		System.out.println(new RotatedArray().search(new ArrayList<>(Arrays.asList(180, 181, 182, 183, 184, 187, 188, 189, 191, 192, 193, 194, 195, 196, 201, 202, 203, 204, 3, 4, 5, 6, 7, 8, 9, 10, 14, 16, 17, 18, 19, 23, 26, 27, 28, 29, 32, 33, 36, 37, 38, 39, 41, 42, 43, 45, 48, 51, 52, 53, 54, 56, 62, 63, 64, 67, 69, 72, 73, 75, 77, 78, 79, 83, 85, 87, 90, 91, 92, 93, 96, 98, 99, 101, 102, 104, 105, 106, 107, 108, 109, 111, 113, 115, 116, 118, 119, 120, 122, 123, 124, 126, 127, 129, 130, 135, 137, 138, 139, 143, 144, 145, 147, 149, 152, 155, 156, 160, 162, 163, 164, 166, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177)), 42));

	}
}
