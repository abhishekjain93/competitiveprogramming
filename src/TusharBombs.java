import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class TusharBombs {

    public static void main(String[] args) {
        System.out.println(new TusharBombs().solve(10, new ArrayList(Arrays.asList(8, 8, 6, 5, 4))));
    }

    public ArrayList<Integer> solve(final int resistance, final ArrayList<Integer> strengths) {

        Collections.reverse(strengths);
        int maxBombs = 0;
        int n = strengths.size();

        int[][] dp = new int[resistance + 1][n];
        boolean[][] isSelected = new boolean[resistance + 1][n];

        dp[0][0] = 0;
        isSelected[0][0] = false;

        for (int i = 0; i <= resistance; i++) {

            dp[i][0] = i / strengths.get(0);

            if (i / strengths.get(0) >= 1)
                isSelected[i][0] = true;
        }

        for (int i = 0; i <= resistance; i++) {
            for (int j = 1; j < n; j++) {

                if (i >= strengths.get(j)) {

                    int curr = 1 + dp[i - strengths.get(j)][j];
                    int prev = dp[i][j - 1];

                    if (curr >= prev) {

                        dp[i][j] = curr;
                        isSelected[i][j] = true;

                    } else {

                        dp[i][j] = prev;
                        isSelected[i][j] = false;

                    }

                } else {

                    dp[i][j] = dp[i][j - 1];
                    isSelected[i][j] = false;
                }

            }
        }

        ArrayList<Integer> persons = new ArrayList<>();

        int p = resistance;
        int q = n - 1;

        while (p >= 0 && q >= 0) {

            if (isSelected[p][q]) {
                persons.add(n - 1 - q); // strengths array is inverted. becouse we want to select min first so we
                // invert and current is given priority in case of equality which is indeed smaller index after reversal
                p = p - strengths.get(q);
            } else {

                q = q - 1;
            }
        }

        //    System.out.println(Arrays.deepToString(dp));
        //    System.out.println(Arrays.deepToString(isSelected));

        return persons;
    }

// using greedy

    public ArrayList<Integer> solve1(int A, ArrayList<Integer> B) {
        int minE = B.get(0);
        for (int i = 0; i < B.size(); i++) {
            minE = Math.min(minE, B.get(i));
        }
        int maxKicks = A / minE;

        ArrayList<Integer> ans = new ArrayList<Integer>();
        int idx = 0;
        while (idx < B.size() && A > 0) {
            if (A - B.get(idx) < 0) {
                idx++;
                continue;
            }
            if (1 + ans.size() + (A - B.get(idx)) / minE < maxKicks) {
                idx++;
                continue;
            }
            ans.add(idx);
            A = A - B.get(idx);
        }

        return ans;
    }
}