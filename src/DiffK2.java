import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DiffK2 {

	public int diffk2(ArrayList<Integer> a, int b) {

		int size = a.size();
		Map<Integer, Integer> map = new HashMap<>();

		for (int i = 0; i < size; i++) {

			map.put(a.get(i), i);
		}

		for (int i = 0; i < size; i++) {

			int target = a.get(i) + b;
			if (map.get(target) != null && map.get(target) != i) {

				System.out.println(map.get(target) + "  " + i);
				return 1;

			}
		}
		return 0;
	}

	public static void main(String[] args) {

		System.out.println(new DiffK2().diffk2(new ArrayList<>(Arrays.asList(5, 7)), 0));

	}
}
