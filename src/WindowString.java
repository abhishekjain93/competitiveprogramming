public class WindowString {

    public static void main(String[] args) {

        System.out.println(new WindowString().windowString("A", "A"));

    }

    public String windowString(String S, String T) {

        int[] h_string = new int[128];
        int[] h_pattern = new int[128];

        for (Character a : T.toCharArray())
            h_pattern[a]++;

        int start = 0, end = 0, counter = 0, min_start = 0, min_end = Integer.MAX_VALUE;


        while (end < S.length()) {

            h_string[S.charAt(end)]++;

            if (h_pattern[S.charAt(end)] >= h_string[S.charAt(end)])
                counter++;

            if (counter == T.length()) {

                if (end - start < min_end - min_start) {

                    min_end = end;
                    min_start = start;
                }

                while (start < S.length() && counter >= T.length() - 1) {

                    h_string[S.charAt(start)]--;
                    if (h_pattern[S.charAt(start)] > h_string[S.charAt(start)])
                        counter--;

                    if (counter >= T.length() - 1)
                        start++;

                    else {
                        h_string[S.charAt(start)]++;
                        counter++;
                        break;
                    }
                    if (counter == T.length()) {

                        if (end - start < min_end - min_start) {

                            min_end = end;
                            min_start = start;
                        }
                    }
                }
            }
            end++;
        }
        if (min_end == Integer.MAX_VALUE) return "";
        return S.substring(min_start, min_end + 1);
    }
}
