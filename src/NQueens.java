import java.util.ArrayList;

public class NQueens {

    public ArrayList<ArrayList<String>> solveNQueens(int n) {

        if (n == 2 || n == 3) return new ArrayList<>();
        ArrayList<ArrayList<String>> board = new ArrayList<>();
        ArrayList<ArrayList<String>> res = new ArrayList<>();

        for (int i = 0; i < n; i++) {

            ArrayList<String> row = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                row.add(".");
            }
            board.add(row);

        }

        solveBoard(board, n, 0, res);
        return res;
    }

    private void solveBoard(ArrayList<ArrayList<String>> board, int n, int col, ArrayList<ArrayList<String>> res) {

        if (col == n) {
            res.add(enumerateBoard(board));

            return;
        }

        for (int row = 0; row < n; row++) {

            if (checkCell(board, n, row, col)) {

                markCell(board, row, col);
                solveBoard(board, n, col + 1, res);
            }
            unMarkCell(board, row, col);
        }
    }

    private ArrayList<String> enumerateBoard(ArrayList<ArrayList<String>> board) {

        ArrayList<String> res = new ArrayList<>();

        for (ArrayList<String> row : board) {
            StringBuilder rowB = new StringBuilder();
            for (String cell : row) {

                rowB.append(cell);

            }
            res.add(rowB.toString());

        }
        return res;
    }

    private boolean checkCell(ArrayList<ArrayList<String>> board, int n, int row, int col) {


        for (int i = 0; i < col; i++) {

            if (board.get(row).get(i).equals("Q")) return false;

        }

        int i = row - 1, j = col - 1;
        while (i >= 0 && j >= 0) {

            if (board.get(i).get(j).equals("Q")) return false;
            i--;
            j--;

        }

        i = row + 1;
        j = col - 1;
        while (i < n && j >= 0) {

            if (board.get(i).get(j).equals("Q")) return false;
            i++;
            j--;

        }
        return true;
    }

    private void unMarkCell(ArrayList<ArrayList<String>> board, int row, int col) {

        board.get(row).set(col, ".");
    }

    private void markCell(ArrayList<ArrayList<String>> board, int row, int col) {

        board.get(row).set(col, "Q");
    }


    public static void main(String[] args) {

        System.out.println(new NQueens().solveNQueens(4));
    }
}
