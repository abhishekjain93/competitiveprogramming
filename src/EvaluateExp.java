import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class EvaluateExp {
	public int evalRPN(ArrayList<String> a) {

		int size = a.size();
		Stack<String> stack = new Stack<>();

		for (int i = 0; i < size; i++) {
			String x = a.get(i);

			if ("/".equals(x) || "*".equals(x) || "+".equals(x) || "-".equals(x)) {

				int op1 = Integer.parseInt(stack.pop());
				int op2 = Integer.parseInt(stack.pop());

				stack.push(Integer.toString(operate(op2, op1, x)));
			} else {
				stack.push(a.get(i));

			}
		}
		return Integer.parseInt(stack.pop());

	}

	public int operate(int x1, int x2, String op) {

		if ("/".equals(op))
			return x1 / x2;
		if ("*".equals(op))
			return x1 * x2;
		if ("+".equals(op))
			return x1 + x2;
		if ("-".equals(op))
			return x1 - x2;

		return 0;
	}

	public static void main(String[] args) {

		System.out.println(
				new EvaluateExp().evalRPN(new ArrayList<>(Arrays.asList("5", "1", "2", "+", "4", "*", "+", "3", "-"))));
	}
}