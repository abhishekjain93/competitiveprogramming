import java.util.Arrays;

public class EvenPermutation {

    public static void swap(final int[] a, final int i, final int j) {
        if (i == j || i < 0 || j < 0 || i > a.length - 1 || j > a.length - 1) {
            return;
        }
        a[i] ^= a[j];
        a[j] ^= a[i];
        a[i] ^= a[j];
    }

    public static int[] nextEven(final int[] digits) {
        int y = digits.length - 1;
        boolean evenFound = digits[y] % 2 == 0;
        // find longest increasing subarray from right to left
        for (int i = digits.length - 2; i >= 0; i--) {
            if (digits[i] >= digits[i + 1]) {
                evenFound |= digits[i] % 2 == 0;
                y = i;
            } else {
                break;
            }
        }

        int maxEven = -1;
        // if y doesn’t contain an even then extend y to left until an even found
        while (!evenFound && y - 1 >= 0 && digits[y - 1] % 2 != 0) {
            y--;
        }

        // input is already the largest permutation
        if (y <= 0) {
            return digits[digits.length - 1] % 2 == 0 ? digits : null;
        }

        //try to extend Y such that y contains an even after swapping X[a] with the Y[b]
        while (y - 1 >= 0) {
            // now X = digits[0..y-1], and Y = digits[y..digits.length-1]
            // a is the rightmost element of x, i.e. a = y-1;
            // find b = min of y greater than a
            final int a = y - 1;
            int b = -1;
            for (int i = y; i < digits.length; i++) {
                b = digits[i] > digits[a] && (b == -1 || (digits[i] < digits[b])) ? i : b;
            }

            // input is already the largest permutation
            if (b == -1) {
                return digits[digits.length - 1] % 2 == 0 ? digits : null;
            }
            // swapPairs a and b
            swap(digits, a, b);

            // update max even in y
            for (int i = y; i < digits.length; i++) {
                maxEven = digits[i] % 2 == 0 && (maxEven == -1 || (maxEven != -1 && digits[i] > digits[maxEven])) ? i
                        : maxEven;
            }

            // input is already the largest permutation or need to extend y
            if (maxEven == -1) {
                y--;
            } else {
                break;
            }
        }

        if (maxEven == -1) {
            return digits[digits.length - 1] % 2 == 0 ? digits : null;
        }

        // swapPairs max even with rightmost position
        swap(digits, maxEven, digits.length - 1);
        // sort y leaving rightmost position unchanged
        Arrays.sort(digits, y, digits.length - 1);

        return digits;
    }

    public static void main(String[] args) {

        System.out.println(Arrays.toString(nextEven(new int[]{1, 8, 3, 5})));

    }
}
