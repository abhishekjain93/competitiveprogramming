import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class CustomerServiceCapacity {

    public static void main(String[] args) {

        CustomerServiceCapacity instance = new CustomerServiceCapacity();
        instance.testRunner(instance.Test00(), 1);
        instance.testRunner(instance.Test01(), 2);
        instance.testRunner(instance.Test02(), 0);
        instance.testRunner(instance.Test03(), 2);
        instance.testRunner(instance.Test04(), 2);
        instance.testRunner(instance.Test05(), 3);

    }

    protected int Test00() {
        int numOfAgent = 1;
        int[][] callsTimes = new int[3][];

        callsTimes[0] = new int[2];
        callsTimes[0][0] = 1481222000;
        callsTimes[0][1] = 1481222020;
        callsTimes[1] = new int[2];
        callsTimes[1][0] = 1481222000;
        callsTimes[1][1] = 1481222040;
        callsTimes[2] = new int[2];
        callsTimes[2][0] = 1481222030;
        callsTimes[2][1] = 1481222035;

        ArrayList<List<Integer>> callList = new ArrayList<>();
        for (int[] array : callsTimes) {
            callList.add(new ArrayList<>(Arrays.stream(array)
                                               .boxed()
                                               .collect(Collectors.toList())));
        }
        return numberOfAgents(numOfAgent, callList);
    }

    private int Test01() {
        int numOfAgent = 1;
        int[][] callsTimes = new int[3][];

        callsTimes[0] = new int[2];
        callsTimes[0][0] = 1481222000;
        callsTimes[0][1] = 1481222020;
        callsTimes[1] = new int[2];
        callsTimes[1][0] = 1481222001;
        callsTimes[1][1] = 1481222040;
        callsTimes[2] = new int[2];
        callsTimes[2][0] = 1481222002;
        callsTimes[2][1] = 1481222035;

        ArrayList<List<Integer>> callList = new ArrayList<>();
        for (int[] array : callsTimes) {
            callList.add(new ArrayList<>(Arrays.stream(array)
                                               .boxed()
                                               .collect(Collectors.toList())));
        }
        return numberOfAgents(numOfAgent, callList);
    }

    private int Test02() {
        int numOfAgent = 1;
        int[][] callsTimes = new int[3][];

        callsTimes[0] = new int[2];
        callsTimes[0][0] = 1481222000;
        callsTimes[0][1] = 1481222010;
        callsTimes[1] = new int[2];
        callsTimes[1][0] = 1481222020;
        callsTimes[1][1] = 1481222030;
        callsTimes[2] = new int[2];
        callsTimes[2][0] = 1481222040;
        callsTimes[2][1] = 1481222050;

        ArrayList<List<Integer>> callList = new ArrayList<>();
        for (int[] array : callsTimes) {
            callList.add(new ArrayList<>(Arrays.stream(array)
                                               .boxed()
                                               .collect(Collectors.toList())));
        }
        return numberOfAgents(numOfAgent, callList);
    }

    private int Test03() {
        int numOfAgent = 1;
        int[][] callsTimes = new int[3][];

        callsTimes[0] = new int[2];
        callsTimes[0][0] = 1481222000;
        callsTimes[0][1] = 1481222050;
        callsTimes[1] = new int[2];
        callsTimes[1][0] = 1481222020;
        callsTimes[1][1] = 1481222050;
        callsTimes[2] = new int[2];
        callsTimes[2][0] = 1481222040;
        callsTimes[2][1] = 1481222050;

        ArrayList<List<Integer>> callList = new ArrayList<>();
        for (int[] array : callsTimes) {
            callList.add(new ArrayList<>(Arrays.stream(array)
                                               .boxed()
                                               .collect(Collectors.toList())));
        }
        return numberOfAgents(numOfAgent, callList);
    }

    private int Test04() {
        int numOfAgent = 1;
        int[][] callsTimes = new int[4][];

        callsTimes[0] = new int[2];
        callsTimes[0][0] = 1481222000;
        callsTimes[0][1] = 1481222050;
        callsTimes[1] = new int[2];
        callsTimes[1][0] = 1481222020;
        callsTimes[1][1] = 1481222050;
        callsTimes[2] = new int[2];
        callsTimes[2][0] = 1481222040;
        callsTimes[2][1] = 1481222050;
        callsTimes[3] = new int[2];
        callsTimes[3][0] = 1481222050;
        callsTimes[3][1] = 1481222060;

        ArrayList<List<Integer>> callList = new ArrayList<>();
        for (int[] array : callsTimes) {
            callList.add(new ArrayList<>(Arrays.stream(array)
                                               .boxed()
                                               .collect(Collectors.toList())));
        }
        return numberOfAgents(numOfAgent, callList);
    }

    private int Test05() {
        int numOfAgent = 1;
        int[][] callsTimes = new int[4][];

        callsTimes[0] = new int[2];
        callsTimes[0][0] = 1481222000;
        callsTimes[0][1] = 1481222050;
        callsTimes[1] = new int[2];
        callsTimes[1][0] = 1481222020;
        callsTimes[1][1] = 1481222050;
        callsTimes[2] = new int[2];
        callsTimes[2][0] = 1481222040;
        callsTimes[2][1] = 1481222050;
        callsTimes[3] = new int[2];
        callsTimes[3][0] = 1481222045;
        callsTimes[3][1] = 1481222060;

        ArrayList<List<Integer>> callList = new ArrayList<>();
        for (int[] array : callsTimes) {
            callList.add(new ArrayList<>(Arrays.stream(array)
                                               .boxed()
                                               .collect(Collectors.toList())));
        }
        return numberOfAgents(numOfAgent, callList);
    }

    protected void testRunner(int actual, int expected) {

        String res = actual == expected ? "PASS" : "FAIL";
        System.out.println(res);

    }

    public int numberOfAgents(int initialAgents, List<List<Integer>> callTimes) {

        TreeMap<Integer, Integer> sortedTimes = new TreeMap<>();

        for (int i = 0; i < callTimes.size(); i++) {

            int arrival = callTimes.get(i)
                                   .get(0);

            int departure = callTimes.get(i)
                                     .get(1);

            if (sortedTimes.containsKey(arrival)) {
                sortedTimes.put(arrival, sortedTimes.get(arrival) + 1);

            } else {

                sortedTimes.put(arrival, 1);

            }
            if (sortedTimes.containsKey(departure)) {
                sortedTimes.put(departure, sortedTimes.get(departure) - 1);

            } else {

                sortedTimes.put(departure, -1);

            }

        }
        int current = 0, max = Integer.MIN_VALUE;

        for (int x : sortedTimes.values()) {

            current += x;
            max = Math.max(max, current);

        }

        return max > initialAgents ? max - initialAgents : 0;
    }
}
