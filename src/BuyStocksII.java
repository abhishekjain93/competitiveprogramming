import java.util.List;

public class BuyStocksII {

    public int maxProfit(final List<Integer> prices) {

        int total = 0;
        for (int i=0; i< prices.size()-1; i++) {
            if (prices.get(i+1)>prices.get(i))
                total += prices.get(i+1)-prices.get(i);
        }

        return total;
    }
}
