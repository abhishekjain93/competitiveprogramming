import java.util.Arrays;
import java.util.List;

public class CoinsOnLine {

    public static void main(String[] args) {

        System.out.println(new CoinsOnLine().maxcoin(Arrays.asList()));

    }

    public int maxcoin(List<Integer> list) {

        int n = list.size();
        if (n < 2) return 0;
        int[][] dpMax = new int[n][n];

        for (int i = 0; i < n; i++) {

            dpMax[i][i] = list.get(i);

        }

        for (int i = 0; i < n - 1; i++) {

            dpMax[i][i + 1] = Math.max(list.get(i), list.get(i + 1));

        }

        for (int gap = 2; gap < n; gap++) {

            for (int i = 0; i + gap < n; i++) {
                int j = i + gap;

                dpMax[i][j] = Math.max(list.get(i) + Math.min(dpMax[i + 2][j], dpMax[i + 1][j - 1]), list.get(j) +
                        Math.min(dpMax[i + 1][j - 1], dpMax[i][j - 2]));

            }

        }

        return dpMax[0][n - 1];
    }

}
