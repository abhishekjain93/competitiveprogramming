public class WaysToDecode {

    public int numDecodings(String message) {

        int length = message.length();
        int[] memo = new int[length + 1];
        for (int i = 0; i < memo.length; i++) {
            memo[i] = -1;
        }
        memo[length] = 1;
        return decodeHelper(message, 0, memo);
    }

    private int decodeHelper(String message, int startIdx, int[] memo) {

        if (memo[startIdx] != -1) return memo[startIdx];

        if (Character.getNumericValue(message.charAt(startIdx)) == 0)
            return memo[startIdx] = 0;

        int ways = 0;

        ways += decodeHelper(message, startIdx + 1, memo);

        if (startIdx < message.length() - 1 && (Character.getNumericValue(message.charAt(startIdx)) == 1 || (Character.getNumericValue(message.charAt(startIdx)) == 2 && Character.getNumericValue(message.charAt(startIdx + 1)) < 7)))

            ways += decodeHelper(message, startIdx + 2, memo);

        return memo[startIdx] = ways;
    }
}
