import java.util.HashMap;

public class IntegerToRoman {


    public String intToRoman(int number) {

        HashMap<Integer, String> map = new HashMap<>();
        map.put(1, "I");
        map.put(4, "IV");
        map.put(5, "V");
        map.put(9, "IX");
        map.put(10, "X");
        map.put(40, "XL");
        map.put(50, "L");
        map.put(90, "XC");
        map.put(100, "C");
        map.put(400, "CD");
        map.put(500, "D");
        map.put(900, "CM");
        map.put(1000, "M");

        int[] lookup = new int[13];

        lookup[0] = 1;
        lookup[1] = 4;
        lookup[2] = 5;
        lookup[3] = 9;
        lookup[4] = 10;
        lookup[5] = 40;
        lookup[6] = 50;
        lookup[7] = 90;
        lookup[8] = 100;
        lookup[9] = 400;
        lookup[10] = 500;
        lookup[11] = 900;
        lookup[12] = 1000;

        int i = 12;
        StringBuilder sBuider = new StringBuilder();

        while (number > 0) {

            while (lookup[i] > number)
                i--;

            number -= lookup[i];
            sBuider.append(map.get(lookup[i]));

        }


        return sBuider.toString();
    }

    public static void main(String[] args) {

        System.out.println(new IntegerToRoman().intToRoman(9));

    }
}
