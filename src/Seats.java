import java.util.Deque;
import java.util.LinkedList;
import java.util.Objects;

public class Seats {

    public static void main(String[] args) {

        System.out.println(new Seats().seats("..xx...xx..xx..xxx..."));

    }

    public int seats(String seating) {

        long result = 0;
        int i = 0, j = 0;

        Deque<Pair> deque = new LinkedList<>();
        for (; j < seating.length(); j++) {

            if (seating.charAt(j) != 'x') {

                if (i != j) {

                    deque.add(new Pair(i, j - 1));
                }
                i = j + 1;
            }
        }

        if (seating.charAt(seating.length() - 1) == 'x') {

            deque.add(new Pair(i, j - 1));
        }

        if (deque.isEmpty()) return 0;

        //System.out.println(deque);

        while (deque.size() != 1) {

            Pair first = deque.peekFirst();
            Pair last = deque.peekLast();

            if (first.y - first.x > last.y - last.x) {

                last = deque.pollLast();
                Pair secondLast = deque.pollLast();

                int gap = last.x - secondLast.y - 1;
                result = (result % 10000003 + (gap * (last.y - last.x + 1)) % 10000003) % 10000003;

                deque.addLast(new Pair(secondLast.x, secondLast.y + last.y - last.x + 1));

            } else {

                first = deque.pollFirst();
                Pair second = deque.pollFirst();

                int gap = second.x - first.y - 1;
                result = (result % 10000003 + (gap * (first.y - first.x + 1)) % 10000003) % 10000003;

                deque.addFirst(new Pair(second.x - (first.y - first.x + 1), second.y));

            }

        }

        return (int) result % 10000003;
    }

    class Pair {
        int x;
        int y;

        Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair pair = (Pair) o;
            return x == pair.x &&
                    y == pair.y;
        }

        @Override
        public int hashCode() {

            return Objects.hash(x, y);
        }

        @Override
        public String toString() {
            return "{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }

    }

}
