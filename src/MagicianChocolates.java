import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;

public class MagicianChocolates {

    public static void main(String[] args) {


        System.out.println(new MagicianChocolates().nchoc(3, new ArrayList<>(Arrays.asList(6, 5))));
    }

    public int nchoc(int k, ArrayList<Integer> bags) {


        double res = 0;
        int MOD = (int) (Math.pow(10, 9) + 7);

        PriorityQueue<Integer> heap = new PriorityQueue<Integer>(bags.size(), Collections.<Integer>reverseOrder());
        heap.addAll(bags);

        for (int i = 0; i < k; i++) {

            int ai = heap.poll();
            res += ai % MOD;
            res %= MOD;
            heap.add((int) Math.floor(ai / 2.0));

        }

        return (int) res;
    }
}
