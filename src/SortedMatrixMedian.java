import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SortedMatrixMedian {
    public int findMedian(ArrayList<ArrayList<Integer>> A) {

        int rowCount = A.size();
        int colCount = A.get(0).size();
        int medianIndex = rowCount * colCount / 2 + 1;
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < rowCount; i++) {

            min = min < A.get(i).get(0) ? min : A.get(i).get(0);
        }

        for (int i = 0; i < rowCount; i++) {

            max = max > A.get(i).get(colCount - 1) ? max : A.get(i).get(colCount - 1);
        }

        while (min < max) {

            int mid = min + (max - min) / 2;
            int count = 0;
            for (int i = 0; i < rowCount; i++) {

                count += upper(A.get(i), mid);

            }
            if (count < medianIndex)
                min = mid + 1;

            else
                max = mid;
        }
        return min;
    }

    private int upper(List<Integer> list, int target) {

        int low = 0, high = list.size() - 1;
        if (list.get(0) > target) return 0;
        while (low < high) {

            int mid = low + (high - low + 1) / 2;
            if (list.get(mid) <= target)
                low = mid;
            else
                high = mid - 1;
        }

        return low + 1;
    }

    public static void main(String[] args) {
        System.out.println(new SortedMatrixMedian().upper(Arrays.asList(3, 5, 5, 6, 9), 2));

    }


}
