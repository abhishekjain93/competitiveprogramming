import java.util.*;

public class CombinationSum {

    public static void main(String[] args) {

        System.out.println(new CombinationSum().combinationSum(new ArrayList<>(Arrays.<Integer>asList(10, 1, 2, 7, 6, 5)), 8));

    }

    public ArrayList<ArrayList<Integer>> combinationSum(ArrayList<Integer> list, int sum) {

        Set<ArrayList<Integer>> res = new HashSet<>();
        Collections.sort(list);
        combinationSum(list, sum, 0, new ArrayList<>(), res);

        ArrayList<ArrayList<Integer>> resArray = new ArrayList<>(res);

        Collections.sort(resArray, new Comparator<ArrayList<Integer>>() {
            @Override
            public int compare(ArrayList<Integer> o1, ArrayList<Integer> o2) {

                int minLen = Math.min(o1.size(), o2.size());

                for (int i = 0; i < minLen; i++) {

                    if (o1.get(i) < o2.get(i)) return -1;
                    if (o1.get(i) > o2.get(i)) return 1;


                }

                return 0;
            }
        });

        return resArray;
    }

    public void combinationSum(ArrayList<Integer> list, int sum, int start, ArrayList<Integer> path, Set<ArrayList<Integer>> res) {

        if (sum == 0) {
            res.add(new ArrayList<>(path));
            return;
        }
        if (sum < 0 || start >= list.size()) return;

        path.add(list.get(start));
        combinationSum(list, sum - list.get(start), start, path, res);
        combinationSum(list, sum - list.get(start), start + 1, path, res);
        path.remove(path.size() - 1);
        combinationSum(list, sum, start + 1, path, res);

    }
}
