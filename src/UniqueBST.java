public class UniqueBST {

    public static void main(String[] args) {

        System.out.println(new UniqueBST().numTrees(2));

    }

    public int numTrees(int nodes) {


        int[] dp = new int[nodes < 4 ? 4 : nodes + 1];
        dp[0] = 1;
        dp[1] = 1;
        dp[2] = 2;
        dp[3] = 5;

        return bstCountHelper(nodes, dp);
    }

    public int bstCountHelper(int nodes, int[] dp) {

        if (dp[nodes] != 0) return dp[nodes];

        int ways = 0;

        for (int i = 0; i < nodes; i++) {

            ways += bstCountHelper(i, dp) * bstCountHelper(nodes - i - 1, dp);

        }

        return dp[nodes] = ways;
    }
}
