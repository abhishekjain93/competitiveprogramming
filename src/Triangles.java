import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Triangles {
	public int nTriang(ArrayList<Integer> a) {

		Collections.sort(a);

		int size = a.size();
		int l = 0, r = 1, n = 0;
		int res = 0;

		while (l < size && a.get(l).equals(0))
			l++;

		while (l < size - 2) {
			r = l + 1;

			while (r < size - 1) {

				int e1 = a.get(l);
				int e2 = a.get(r);

				n = bSearch(a, e1 + e2);

				if (n == -1)
					res += 0;

				else if (a.get(n).equals(e1 + e2))
					res += n - r - 1;

				else
					res += n - r;

				r++;
			}
			l++;
		}

		return res;
	}

	public int bSearch(ArrayList<Integer> a, int b) {

		int start = 0;
		int end = a.size() - 1;
		int mid = 0;
		int first = -1;

		while (start <= end) {
			mid = (start + end) / 2;

			if (a.get(mid) < b) {

				start = mid + 1;
				first = mid;
				continue;
			} else if (a.get(mid) > b) {

				end = mid - 1;
				continue;
			} else {

				first = mid;
				if (mid == 0 || a.get(mid - 1) < b)
					break;
				end = mid - 1;

			}

		}
		return first;
	}

	public static void main(String[] args) {

		System.out.println(new Triangles().nTriang(new ArrayList<>(Arrays.asList(2,3,6))));

	}
}
