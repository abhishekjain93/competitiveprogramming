import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LargestDistanceParentArray {

    public static void main(String[] args) {

        System.out.println(new LargestDistanceParentArray().solve(new int[]{-1}));

    }

    public int solve(int[] tree) {

        int max = 0;

        Map<Integer, List<Integer>> childMap = new HashMap<>();

        for (int i = 0; i < tree.length; i++) {

            if (tree[i] == -1) continue;
            if (!childMap.containsKey(tree[i])) {

                List<Integer> children = new ArrayList<>();
                children.add(i);
                childMap.put(tree[i], children);

            } else {

                childMap.get(tree[i])
                        .add(i);
            }
        }

        int[] heights = new int[tree.length];
        //System.out.println(childMap);

        for (int i = 0; i < tree.length; i++) {
            height(i, heights, childMap);
        }

        //System.out.println(Arrays.toString(heights));

        for (int i = 0; i < tree.length; i++) {

            if (childMap.containsKey(i)) {
                List<Integer> children = childMap.get(i);

                int max1 = 0, max2 = 0;
                for (int j = 0; j < children.size(); j++) {

                    if (heights[children.get(j)] > max1) {

                        max2 = max1;
                        max1 = heights[children.get(j)];

                    } else if (heights[children.get(j)] > max2) {

                        max2 = heights[children.get(j)];
                    }

                }

                max = Math.max(max, 1 + max1 + max2);
            }
        }

        return max == 0 ? 0 : max - 1;

    }

    private int height(int node, int[] heights, Map<Integer, List<Integer>> childMap) {

        if (heights[node] != 0) return heights[node];

        if (!childMap.containsKey(node)) return heights[node] = 1;

        List<Integer> children = childMap.get(node);

        Integer maxChildHeight = 0;

        for (int j = 0; j < children.size(); j++) {

            maxChildHeight = Math.max(maxChildHeight, height(children.get(j), heights, childMap));

        }
        return heights[node] = maxChildHeight + 1;

    }
}
