import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Triplets {
	// (0,0.5) (0.5, 0.666666666) (0.666666666,1) (1,2)
	public int solve(ArrayList<String> input) {

		int size = input.size();
		double twoThird = fractionToDouble("2/3");

		ArrayList<Double> x = new ArrayList<>();
		ArrayList<Double> y = new ArrayList<>();
		ArrayList<Double> z = new ArrayList<>();

		for (int i = 0; i < size; i++) {

			double val = Double.parseDouble(input.get(i));
			if (val < twoThird) {
				x.add(val);
			} else if (val < 1) {
				y.add(val);

			} else if (val < 2) {
				z.add(val);

			}
		}
		Collections.sort(x);
		Collections.sort(y);
		Collections.sort(z);
		int xlen = x.size();
		int ylen = y.size();
		int zlen = z.size();

		/*
		 * |X| >= 3 and Xmax(1) + Xmax(2) + Xmax(3) >= 1 |X| >= 2, |Z| >= 1, and
		 * Xmin(1)+Xmin(2)+Zmin(1) <= 2 |X| >= 1, |Y| >= 2, and
		 * Xmin(1)+Ymin(1)+Ymin(2) <= 2 |X| >= 1, |Y| >= 1, |Z| >= 1, and
		 * Xmin(1)+Ymin(1)+Zmin(1) <= 2 |X| >= 2, |Y| >= 1, and Xmax(1) +
		 * Xmax(2) + Ymin(1) < 2 |X| >= 2, |Y| >= 1, and Xmin(1) + Xmin(2) +
		 * Ymax(1) > 1
		 * 
		 */

		if (xlen >= 3 && (x.get(xlen - 1) + x.get(xlen - 2) + x.get(xlen - 3) > 1.0))
			return 1;

		if (xlen >= 2 && zlen >= 1 && (x.get(0) + x.get(1) + z.get(0) < 2.0))
			return 1;

		if (xlen >= 1 && ylen >= 2 && (x.get(0) + y.get(0) + y.get(1) < 2.0))
			return 1;

		if (xlen >= 1 && ylen >= 1 && zlen >= 1 && (x.get(0) + y.get(0) + z.get(0) < 2.0))
			return 1;

		ArrayList<Double> x1 = new ArrayList<>();
		ArrayList<Double> x2 = new ArrayList<>();

		for (int l = 0; l < xlen; l++) {

			if (x.get(l) < 0.5)
				x1.add(x.get(l));

			else
				x2.add(x.get(l));

		}

		int x1len = x1.size();
		int x2len = x2.size();

		if (x1len >= 2 && ylen >= 1 && (x1.get(x1len - 1) + x1.get(x1len - 2) + y.get(ylen - 1) > 1.0))
			return 1;

		if (x2len >= 2 && ylen >= 1 && (x2.get(0) + x2.get(1) + y.get(0) < 2.0))
			return 1;

		if (x2len >= 1 && x1len >= 1 && ylen >= 1 && (x1.get(0) + x2.get(0) + y.get(0) < 2.0))
			return 1;

		return 0;
	}

	public static double fractionToDouble(String fraction) {
		Double d = null;
		if (fraction != null) {
			if (fraction.contains("/")) {
				String[] numbers = fraction.split("/");
				if (numbers.length == 2) {
					java.math.BigDecimal d1 = java.math.BigDecimal.valueOf(Double.valueOf(numbers[0]));
					java.math.BigDecimal d2 = java.math.BigDecimal.valueOf(Double.valueOf(numbers[1]));
					java.math.BigDecimal response = d1.divide(d2, java.math.MathContext.DECIMAL128);
					d = response.doubleValue();
				}
			} else {
				d = Double.valueOf(fraction);
			}
		}
		return d;
	}

	public static void main(String[] args) {
		System.out.println(new Triplets().solve(new ArrayList<>(Arrays.asList("0.503094", "0.648924", "0.999298"))));
	}

}
