import java.util.ArrayList;
import java.util.List;

public class GrayCode {


    public static void main(String[] args) {

        System.out.println(new GrayCode().grayCode(2));

    }

    public ArrayList<Integer> grayCode(int n) {

        ArrayList<Integer> res = new ArrayList<>();
        res.add(0);
        res.add(1);

        int adder = 2;
        for (int i = 2; i <= n; i++) {

            List<Integer> reversed = reverse(res);
            for (int j = 0; j < adder; j++) {
                reversed.set(j, reversed.get(j) + adder);

            }
            adder *= 2;
            res.addAll(reversed);

        }


        return res;
    }

    static <T> List<T> reverse(final List<T> list) {
        final int size = list.size();
        final int last = size - 1;

        final List<T> result = new ArrayList<>(size);

        for (int i = last; i >= 0; --i) {
            final T element = list.get(i);
            result.add(element);
        }

        return result;
    }


}
