public class ReorderList {


    public static void main(String[] args) {

        ListNode n1 = new ListNode(1);
        ListNode n2 = new ListNode(2);
        ListNode n3 = new ListNode(3);
        ListNode n4 = new ListNode(4);
        ListNode n5 = new ListNode(5);

        //n1.next = n2;
        //n2.next = n3;
        //n3.next = n4;
      //  n4.next = n5;

        System.out.println(n1);
        System.out.println(new ReorderList().reorderList(n1));

    }


    public ListNode reorderList(ListNode A) {

        ListNode start = A, p = A, q = A;
        boolean isEven = true;

        while (q.next != null && q.next.next != null) {

            p = p.next;
            q = q.next.next;

        }

        if (q.next == null)
            isEven = false;

        ListNode x = p, y = p.next, z = null;
        if (p.next != null)
            z = p.next.next;

        while (y != null) {

            y.next = x;
            x = y;
            y = z;

            if (y != null)
                z = y.next;

        }

        ListNode end = x;
        while (start != p) {

            ListNode a = start.next;
            ListNode b = end.next;
            start.next = end;
            end.next = a;
            start = a;
            end = b;

        }

        if (isEven)
            end.next = null;

        else
            start.next = null;


        return A;
    }
}
