import java.util.*;
import java.util.stream.Collectors;

public class GenerateparenthesisII {

    public static void main(String[] args) {

        System.out.println(new GenerateparenthesisII().generateParenthesis(4));

    }

    public ArrayList<String> generateParenthesis(int n) {

        ArrayList<String> res = parenthesisHelper(n);
        Set<String> set = new TreeSet<>(new ParenthesisComaparator());
        set.addAll(res);

        return new ArrayList<>(set.stream().collect(Collectors.toList()));
    }

    public ArrayList<String> parenthesisHelper(int n) {


        if (n <= 0) return new ArrayList<>(Arrays.asList(""));
        if (n == 1)
            return new ArrayList<>(Arrays.asList("()"));

        Set<String> set = new HashSet<>(n * 2);
        for (String s : generateParenthesis(n - 1)) {
            char[] ca = s.toCharArray();
            int len = s.length();
            for (int i = 0; i < len; i++) {
                set.add(String.copyValueOf(ca, 0, i) + "()"
                        + String.copyValueOf(ca, i, len - i));
            }
        }
        return new ArrayList<String>(set);
    }


    class ParenthesisComaparator implements Comparator<String> {


        @Override
        public int compare(String o1, String o2) {

            int idx1 = o2.indexOf(')');
            int idx2 = o1.indexOf(')');

            if (idx1 != idx2)
                return Integer.compare(o2.indexOf(')'), o1.indexOf(')'));

            if (idx1 >= o1.length() - 2) return 0;
            return compare(o1.substring(idx1 + 1), o2.substring(idx2 + 1));

        }
    }

}
