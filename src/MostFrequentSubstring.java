import java.util.HashMap;
import java.util.Map;

public class MostFrequentSubstring {

    public static void main(String[] args) {

        System.out.println(MostFrequentSubstring.getMaxOccurrences("ababab", 2, 3, 4));

    }

    public static int getMaxOccurrences(String s, int minLength, int maxLength, int maxUnique) {

        int len = s.length();

        int[][] freqCount = new int[len][26];

        Map<String, Integer> sMap = new HashMap();

        freqCount[0][s.charAt(0) - 'a'] = 1;

        for (int i = 1; i < len; i++) {
            for (int j = 'a'; j <= 'z'; j++) {
                freqCount[i][j - 'a'] = freqCount[i - 1][j - 'a'];

            }
            freqCount[i][s.charAt(i) - 'a']++;
        }

        for (int i = 0; i <= s.length() - minLength; i++) {
            int uniqueCount = 0;
            for (char c = 'a'; c <= 'z'; c++) {

                if (i == 0) {

                    if ((freqCount[i + minLength - 1][c - 'a']) > 0) {
                        uniqueCount++;
                    }

                } else {

                    if ((freqCount[i + minLength - 1][c - 'a'] - freqCount[i - 1][c - 'a']) > 0) {
                        uniqueCount++;
                    }

                }
            }

            if (uniqueCount <= maxUnique) {

                StringBuilder hashValue = new StringBuilder();

                for (int j = i; j <= i + minLength - 1; j++) {

                    hashValue.append('.')
                             .append(s.charAt(j));
                }

                String v = hashValue.toString();
                if (sMap.containsKey(v)) {
                    sMap.put(v, sMap.get(v) + 1);
                } else {
                    sMap.put(v, 1);
                }

            }
        }

        int max = 0;
        for (Integer v : sMap.values()) {

            max = Math.max(max, v);
        }

        return max;
    }
}
