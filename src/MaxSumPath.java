public class MaxSumPath {

    private int maxSum = Integer.MIN_VALUE;

    public int maxPathSum(TreeNode root) {

        maxSumPathUtil(root);
        return maxSum;

    }

    private int maxSumPathUtil(TreeNode root) {

        if (root == null) return 0;

        int l = maxSumPathUtil(root.left);
        int r = maxSumPathUtil(root.right);

        maxSum = Math.max(maxSum, Math.max(root.val, root.val + Math.max(l + r, Math.max(l, r))));

        return Math.max(Math.max(l, r) + root.val, root.val);
    }

}
