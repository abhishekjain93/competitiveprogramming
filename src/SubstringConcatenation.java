import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class SubstringConcatenation {

	public ArrayList<Integer> findSubstring(String a, ArrayList<String> b) {

		ArrayList<Integer> res = new ArrayList<>();
		int total = b.size();
		int length = a.length();
		int wordLength = b.get(0).length();

		HashMap<String, Integer> wordMap = new HashMap<>();

		for (String s : b) {
			if (wordMap.containsKey(s))
				wordMap.put(s, wordMap.get(s) + 1);
			else
				wordMap.put(s, 1);

		}

		for (int i = 0; i < wordLength; i++) {

			HashMap<String, Integer> curMap = new HashMap<>(wordMap);

			for (int j = i, begin = i; j <= length - wordLength;) {

				if (begin + total * wordLength > length)
					break;

				String sub = a.substring(j, j + wordLength);

				if (!wordMap.containsKey(sub)) {

					begin = j + wordLength;
					j = begin;
					curMap.putAll(wordMap);

				} else if (!curMap.containsKey(sub)) {

					while (!a.substring(begin, begin + wordLength).equals(sub)) {

						String x = a.substring(begin, begin + wordLength);
						if (curMap.containsKey(x)) {
							curMap.put(x, curMap.get(x) + 1);

						} else
							curMap.put(x, 1);

						begin += wordLength;
					}
					begin += wordLength;
					curMap.put(sub, 1);

				} else {

					if (curMap.get(sub).equals(1))
						curMap.remove(sub);
					else
						curMap.put(sub, curMap.get(sub) - 1);
					j = j + wordLength;

					if (curMap.isEmpty()) {

						res.add(begin);
						String x = a.substring(begin, begin + wordLength);

						curMap.put(x, 1);
						begin = begin + wordLength;

					}
				}
			}
		}
		return res;
	}

	public static void main(String[] args) {

		System.out.println(
				new SubstringConcatenation().findSubstring("qq", new ArrayList(Arrays.asList("bar", "foo"))));
	}
}
