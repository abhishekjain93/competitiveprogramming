import java.util.ArrayList;
import java.util.Arrays;

public class MaxUnsortedSubArray {

	public ArrayList<Integer> subUnsort(ArrayList<Integer> input) {

		int size = input.size();
		int l = 0, r = size - 1;

		while (l < size - 1 && input.get(l) <= input.get(l + 1))
			l++;

		while (r > 0 && input.get(r) >= input.get(r - 1))
			r--;

		if (l == size - 1 || r == 0)
			return new ArrayList<>(Arrays.asList(-1));

		int a_max = input.get(l), a_min = input.get(l);

		for (int i = l; i <= r; i++) {

			if (input.get(i) < a_min) {
				a_min = input.get(i);

			} else if (input.get(i) > a_max) {
				a_max = input.get(i);

			}
		}

		for (int i = 0; i < l; i++) {
			if (input.get(i) > a_min) {

				l = i;
				break;
			}
		}

		for (int i = size - 1; i > r; i--) {
			if (input.get(i) < a_max) {

				r = i;
				break;
			}
		}

		return new ArrayList<>(Arrays.asList(l, r));
	}

	public static void main(String[] args) {

		System.out.println(new MaxUnsortedSubArray()
				.subUnsort(new ArrayList<>(Arrays.asList(1, 3, 2, 4, 5))));
	}
}
