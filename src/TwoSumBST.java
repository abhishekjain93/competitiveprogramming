import java.util.HashSet;
import java.util.Set;

public class TwoSumBST {

    public int t2Sum(TreeNode root, int sum) {

        HashSet<Integer> presentValues = new HashSet<>();
        return twoSumHepler(root, sum, presentValues);
    }

    public int twoSumHepler(TreeNode root, int sum, Set<Integer> presentValues) {

        if (root == null) return 0;
        if (presentValues.contains(sum - root.val)) return 1;

        presentValues.add(root.val);
        return twoSumHepler(root.left, sum, presentValues) | twoSumHepler(root.right, sum, presentValues);

    }

}
