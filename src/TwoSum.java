import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class TwoSum {
	public ArrayList<Integer> twoSum(final List<Integer> a, int b) {

		int size = a.size();

		ArrayIndexComparator comparator = new ArrayIndexComparator(a);
		Integer[] indexes = comparator.createIndexArray();
		Arrays.sort(indexes, comparator);
		ArrayList<Integer> res = new ArrayList<>(2);
		res.add(0, -1);
		res.add(1, -1);
		int minI = size, minJ = size;

		for (int i = 0, j = size - 1; i < j;) {

			if (a.get(indexes[i]) + a.get(indexes[j]) == b) {

				int x = a.get(indexes[i]);
				int y = a.get(indexes[j]);

				int I = size;
				int J = size;
				int l = size;
				int r = size;
				if (x == y) {

					while (a.get(indexes[i]) == x) {
						if (indexes[i] < I) {
							J = I;
							I = indexes[i];
						} else if (indexes[i] < J) {
							J = indexes[i];

						}
						i++;
					}
					l = I;
					r = J;

				} else {

					while (i < size - 1 && a.get(indexes[i]) == x)
						I = Math.min(I, indexes[i++]);

					while (j > 0 && a.get(indexes[j]) == y)
						J = Math.min(J, indexes[j--]);

					l = Math.min(I, J);
					r = I + J - l;
				}
				if (r < minJ || (r == minJ && l < minI)) {
					minI = l;
					minJ = r;
					res.set(0, minI + 1);
					res.set(1, minJ + 1);
				}
			} else if (a.get(indexes[i]) + a.get(indexes[j]) < b)
				i++;
			else
				j--;
		}
		if (res.get(0).equals(-1))
			return new ArrayList<>(2);
		return res;

	}

	public static void main(String[] args) {

		System.out.println(new TwoSum().twoSum(new ArrayList<>(Arrays.asList(2, -9, -5, -8, -4, -2, 1, 1, 0, 6, -3, 9,
				-8, 2, -2, 9, 7, 6, 9, 1, 4, 1, 0, -5, -1, -4, 2, 2, 0, -2, 3, -4, -2, 1, 7, 5, -3, -1, -3, 6, 2, 6, 8,
				-6, -9, 7, 1, 6, -6, -6, -5, -6, -6, 7, -9, 8, -4, -1, 9, -7, 10, -5, -1, 10, 3, 2, -5, -4, 10, -10, 5,
				-2, 10, -3, 5, 3, 4, 9, 0, 0, -9)), 4));
	}

}

class ArrayIndexComparator implements Comparator<Integer> {
	private final List<Integer> array;

	public ArrayIndexComparator(List<Integer> array) {
		this.array = array;
	}

	public Integer[] createIndexArray() {
		int size = array.size();
		Integer[] indexes = new Integer[size];
		for (int i = 0; i < size; i++) {
			indexes[i] = i; // Autoboxing
		}
		return indexes;
	}

	@Override
	public int compare(Integer index1, Integer index2) {
		// Autounbox from Integer to int to use as array indexes
		return array.get(index1).compareTo(array.get(index2));
	}

}