public class KthSmallestElement {

    int counter = 0;
    int result = Integer.MIN_VALUE;

    public int kthsmallest(TreeNode root, int k) {

        smallestHelper(root, k);
        return result;
    }

    private void smallestHelper(TreeNode root, int k) {

        if (root == null) return;
        smallestHelper(root.left, k);
        counter++;
        if (counter == k) {
            result = root.val;
            return;
        }
        smallestHelper(root.right, k);

    }
}
